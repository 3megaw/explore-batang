package com.example.explorebatang

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.Data.Data_DayaTarik

//(listData: List<DataModel_Rule>, listData2: List<Data_DayaTarik>)
class UJI_Adapter  : RecyclerView.Adapter<UJI_Adapter.ViewHolder>() {
    /*private val listData: List<DataModel_Rule>
    private val listData2: List<Data_DayaTarik>*/

    lateinit var SubjectValues: Array<String>
    var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(com.example.explorebatang.R.layout.list_uji, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ld: String = SubjectValues[position]
        //val ld2: Data_DayaTarik = listData2[position]
        holder.txtid.setText(ld)
//        holder.txtname.setText(ld2.nama_dayatarik)
    }

    override fun getItemCount(): Int {
        return SubjectValues.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtid: TextView
        val txtname: TextView
        val txtmovie: TextView

        init {
            txtid = itemView.findViewById(com.example.explorebatang.R.id.idtxt)
            txtname = itemView.findViewById(com.example.explorebatang.R.id.nametxt)
            txtmovie = itemView.findViewById(com.example.explorebatang.R.id.movietxt)
        }
    }

   /* init {
        this.listData = listData
        this.listData2 = listData2
    }*/
}