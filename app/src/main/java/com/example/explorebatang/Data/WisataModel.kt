package com.example.explorebatang.Data

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.DataSnapshot

class WisataModel() : Parcelable {
    var deskripsi: String = ""
    var fasilitas: String = ""
    var image_wisata: String = ""
    var jenis_wisata: String = ""
    var ketinggian: String = ""
    var kode_wisata: String = ""
    var lat_wisata: String = ""
    var lokasi_wisata: String = ""
    var long_wisata: String = ""
    var nama_wisata: String = ""
    var tiket: String = ""
    var tiketdetail: String = ""
    var wakturamai: String = ""

    constructor(parcel: Parcel) : this() {
        deskripsi = parcel.readString().toString()
        fasilitas = parcel.readString().toString()
        image_wisata = parcel.readString().toString()
        jenis_wisata = parcel.readString().toString()
        ketinggian = parcel.readString().toString()
        kode_wisata = parcel.readString().toString()
        lat_wisata = parcel.readString().toString()
        lokasi_wisata = parcel.readString().toString()
        long_wisata = parcel.readString().toString()
        nama_wisata = parcel.readString().toString()
        tiket = parcel.readString().toString()
        tiketdetail = parcel.readString().toString()
        wakturamai = parcel.readString().toString()
    }

    fun parseToModel(postSnapshot4: DataSnapshot) {
        this.deskripsi = (postSnapshot4.child("deskripsi").value as String?).toString()
        this.fasilitas = (postSnapshot4.child("fasilitas").value as String?).toString()
        this.image_wisata = (postSnapshot4.child("image_wisata").value as String?).toString()
        this.jenis_wisata = (postSnapshot4.child("jenis_wisata").value as String?).toString()
        this.ketinggian = (postSnapshot4.child("ketinggian").value as String?).toString()
        this.kode_wisata = (postSnapshot4.child("kode_wisata").value as String?).toString()
        this.lat_wisata = (postSnapshot4.child("lat_wisata").value as String?).toString()
        this.lokasi_wisata = (postSnapshot4.child("lokasi_wisata").value as String?).toString()
        this.long_wisata = (postSnapshot4.child("long_wisata").value as String?).toString()
        this.nama_wisata = (postSnapshot4.child("nama_wisata").value as String?).toString()
        this.tiket = (postSnapshot4.child("tiket").value as String?).toString()
        this.tiketdetail = (postSnapshot4.child("tiketdetail").value as String?).toString()
        this.wakturamai = (postSnapshot4.child("wakturamai").value as String?).toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(deskripsi)
        parcel.writeString(fasilitas)
        parcel.writeString(image_wisata)
        parcel.writeString(jenis_wisata)
        parcel.writeString(ketinggian)
        parcel.writeString(kode_wisata)
        parcel.writeString(lat_wisata)
        parcel.writeString(lokasi_wisata)
        parcel.writeString(long_wisata)
        parcel.writeString(nama_wisata)
        parcel.writeString(tiket)
        parcel.writeString(tiketdetail)
        parcel.writeString(wakturamai)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WisataModel> {
        override fun createFromParcel(parcel: Parcel): WisataModel {
            return WisataModel(parcel)
        }

        override fun newArray(size: Int): Array<WisataModel?> {
            return arrayOfNulls(size)
        }
    }
}
