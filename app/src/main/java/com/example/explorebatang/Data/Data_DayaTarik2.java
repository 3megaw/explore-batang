package com.example.explorebatang.Data;

public class Data_DayaTarik2 {
    private String kode_dayatarik,nama_dayatarik;
    private Boolean selected=false;

    public Data_DayaTarik2(){

    }

    public Data_DayaTarik2(String kode_dayatarik, String nama_dayatarik, Boolean selected) {
        this.kode_dayatarik = kode_dayatarik;
        this.nama_dayatarik = nama_dayatarik;
        this.selected = selected;
    }

    public String getKode_dayatarik() {
        return kode_dayatarik;
    }

    public void setKode_dayatarik(String kode_dayatarik) {
        this.kode_dayatarik = kode_dayatarik;
    }

    public String getNama_dayatarik() {
        return nama_dayatarik;
    }

    public void setNama_dayatarik(String nama_dayatarik) {
        this.nama_dayatarik = nama_dayatarik;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
