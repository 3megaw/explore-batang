package com.example.explorebatang.Data

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper

class Sqlite_DatabaseHandler(context: Context): SQLiteOpenHelper(context,DATABASE_NAME,null,DATABASE_VERSION) {
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "WisataDatabase"
        private val TABLE_CONTACTS = "WisataTable"
        private val KEY_NAME = "nama"
        private val KEY_LAT = "lat"
        private val KEY_LONG = "lon"
    }
    override fun onCreate(db: SQLiteDatabase?) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        //creating table with fields
        val CREATE_CONTACTS_TABLE = ("CREATE TABLE " + TABLE_CONTACTS + "( WISATAID INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT," + KEY_LAT + " TEXT," + KEY_LONG + " TEXT" +")")
        db?.execSQL(CREATE_CONTACTS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS)
        onCreate(db)
    }


    //method to insert data
    fun addWisata(emp: Sqlite_Model):Long{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_NAME, emp.nama)
        contentValues.put(KEY_LAT, emp.lat) // EmpModelClass Name
        contentValues.put(KEY_LONG,emp.lon ) // EmpModelClass Phone
        // Inserting Row
        val success = db.insert(TABLE_CONTACTS, null, contentValues)
        //2nd argument is String containing nullColumnHack
        db.close() // Closing database connection
        return success
    }
    //method to read data
    fun viewEmployee():List<Sqlite_Model>{
        val empList:ArrayList<Sqlite_Model> = ArrayList<Sqlite_Model>()
        val selectQuery = "SELECT  * FROM $TABLE_CONTACTS"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        var nama: String
        var lat: String
        var lon: String
        if (cursor.moveToFirst()) {
            do {
                nama = cursor.getString(cursor.getColumnIndex("nama"))
                lat = cursor.getString(cursor.getColumnIndex("lat"))
                lon = cursor.getString(cursor.getColumnIndex("lon"))
                val emp= Sqlite_Model(nama = nama, lat = lat, lon = lon)
                empList.add(emp)
            } while (cursor.moveToNext())
        }
        return empList
    }
    //method to update data
    /*   fun updateEmployee(emp: EmpModelClass):Int{
           val db = this.writableDatabase
           val contentValues = ContentValues()
           contentValues.put(KEY_ID, emp.userId)
           contentValues.put(KEY_NAME, emp.userName) // EmpModelClass Name
           contentValues.put(KEY_EMAIL,emp.userEmail ) // EmpModelClass Email

           // Updating Row
           val success = db.update(TABLE_CONTACTS, contentValues,"id="+emp.userId,null)
           //2nd argument is String containing nullColumnHack
           db.close() // Closing database connection
           return success
       }
       //method to delete data
       fun deleteEmployee(emp: EmpModelClass):Int{
           val db = this.writableDatabase
           val contentValues = ContentValues()
           contentValues.put(KEY_ID, emp.userId) // EmpModelClass UserId
           // Deleting Row
           val success = db.delete(TABLE_CONTACTS,"id="+emp.userId,null)
           //2nd argument is String containing nullColumnHack
           db.close() // Closing database connection
           return success
       }*/
}