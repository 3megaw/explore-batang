package com.example.explorebatang.Data

class Data_Wisata{
    var deskripsi : String? = null
    var fasilitas : String? = null
    var image_wisata : String? = null
    var jenis_wisata : String? = null
    var ketinggian : String? = null
    var kode_wisata : String? = null
    var lat_wisata : String? = null
    var lokasi_wisata : String? = null
    var long_wisata : String? = null
    var nama_wisata : String? = null
    var tiket : String? = null
    var tiketdetail : String? = null
    var wakturamai : String? = null

    constructor(){
    }

    constructor(deskripsi: String?, fasilitas: String?, image_wisata: String?, jenis_wisata: String?, ketinggian: String?, kode_wisata: String?, lat_wisata: String?, lokasi_wisata: String?, long_wisata: String?, nama_wisata: String?, tiket: String?, tiketdetail: String?, wakturamai: String?) {
        this.deskripsi = deskripsi
        this.fasilitas = fasilitas
        this.image_wisata = image_wisata
        this.jenis_wisata = jenis_wisata
        this.ketinggian = ketinggian
        this.kode_wisata = kode_wisata
        this.lat_wisata = lat_wisata
        this.lokasi_wisata = lokasi_wisata
        this.long_wisata = long_wisata
        this.nama_wisata = nama_wisata
        this.tiket = tiket
        this.tiketdetail = tiketdetail
        this.wakturamai = wakturamai
    }


}