package com.example.explorebatang.Data

class DataModel_Komentar {
    var idkomentar:String?=null
    var iduser:String?=null
    var komentar:String?=null
    var wisata:String?=null
    var tanggal:String?=null

    constructor(){
    }

    constructor(idkomentar: String?, iduser: String?, komentar: String?, wisata: String?, tanggal:String?) {
        this.idkomentar = idkomentar
        this.iduser = iduser
        this.komentar = komentar
        this.wisata = wisata
        this.tanggal = tanggal
    }

}