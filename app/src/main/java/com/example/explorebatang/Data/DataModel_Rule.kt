package com.example.explorebatang.Data

class DataModel_Rule {

    var id_rule:Long? = null
    var kode_dayatarik:String? = null
    var kode_wisata:String? = null
    var nilai_cf:String? = null

    constructor(){
    }

    constructor(id_rule: Long, kode_dayatarik: String?, kode_wisata: String?, nilai_cf: String?) {
        this.id_rule = id_rule
        this.kode_dayatarik = kode_dayatarik
        this.kode_wisata = kode_wisata
        this.nilai_cf = nilai_cf
    }


}