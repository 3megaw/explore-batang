package com.example.explorebatang.Data

class Data_JenisWisata {
    var namajeniswisata : String? = null
    var deskripsijeniswisata: String? = null
    var fotojeniswisata: String? = null
    var pass: String? = null

    constructor(){}
    constructor(namajeniswisata: String?, deskripsijeniswisata: String?, fotojeniswisata: String?, pass:String?) {
        this.namajeniswisata = namajeniswisata
        this.deskripsijeniswisata = deskripsijeniswisata
        this.fotojeniswisata = fotojeniswisata
        this.pass = pass
    }
}