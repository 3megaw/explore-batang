package com.example.explorebatang

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.*
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_adm__add_destinasi.*
import kotlinx.android.synthetic.main.activity_adm__dayatarik_add.*
import java.util.HashMap

class Adm_DayatarikAdd : AppCompatActivity() {

    private var tampungkey : Int? = null
    private var keyString : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm__dayatarik_add)

        getLastKodeRule()

        btn_adddayatarik_unggah.setOnClickListener {
                    val datastore = keyString?.let { it1 -> FirebaseDatabase.getInstance().getReference("CF_DayaTarik").child(it1) }
                    val hashMap = HashMap<String, String>()
                    hashMap["kode_dayatarik"] = edt_adddayatarik_kodedayatarik.getText().toString().trim { it <= ' ' }
                    hashMap["nama_dayatarik"] = edt_adddayatarik_namadayatarik.getText().toString().trim { it <= ' ' }

                    if (datastore != null) {
                        datastore.setValue(hashMap).addOnSuccessListener {
                            Toast.makeText(this, "Terunggah", Toast.LENGTH_SHORT).show()
                            hashMap.clear()
                        }
                    }
                    getLastKodeRule()
                    clearEDT()
        }
        btnback_dayatarikadd.setOnClickListener {
            finish()
        }
    }

    private fun clearEDT() {
        edt_adddayatarik_kodedayatarik.setText("")
        edt_adddayatarik_namadayatarik.setText("")
    }

    private fun getLastKodeRule(){
        var dbref = FirebaseDatabase.getInstance().getReference().child("CF_DayaTarik")
        var lastquery: Query = dbref.orderByKey().limitToLast(1)
        lastquery.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (data in dataSnapshot.children) {
                    tampungkey = data.key?.toInt()
                }
                keyString = (tampungkey?.plus(1)).toString()
                edt_adddayatarik_kodedayatarik.setText("T"+keyString)
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }
}