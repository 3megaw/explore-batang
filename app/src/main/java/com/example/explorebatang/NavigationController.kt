package com.example.explorebatang

import android.content.Context
import android.content.Intent
import com.example.explorebatang.Data.WisataModel

object NavigationController {
    const val EXTRA_LIST_SELECTED_POSSIBILITY = "EXTRA_LIST_SELECTED_POSSIBILITY"
    const val EXTRA_LIST_SELECTED_DAYA_TARIK = "EXTRA_LIST_SELECTED_DAYA_TARIK"
    const val EXTRA_WISATA_MODEL = "EXTRA_WISATA_NAME"

    fun navigateToPendukungKeputusanHasil(context: Context, listSelectedDayaTarik : ArrayList<String>, listSelectedPossibilities : ArrayList<String>){
        val intent = Intent(context, PendukungKeputusan_Hasil::class.java)
        intent.putStringArrayListExtra(EXTRA_LIST_SELECTED_DAYA_TARIK, listSelectedDayaTarik)
        intent.putStringArrayListExtra(EXTRA_LIST_SELECTED_POSSIBILITY, listSelectedPossibilities)
        context.startActivity(intent)
    }

    fun navigateToWisataDetail(
        context: Context,
        wisataModel: WisataModel
    ) {
        val intent = Intent(context, Wisata_Detail::class.java)
        intent.putExtra(EXTRA_WISATA_MODEL, wisataModel)
        context.startActivity(intent)
    }
}