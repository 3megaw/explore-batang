package com.example.explorebatang

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.explorebatang.adapter.Adapter_ListTersimpan
import com.example.explorebatang.Data.Sqlite_DatabaseHandler
import com.example.explorebatang.Data.Sqlite_Model
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_tersimpan_wisata.*


class TersimpanWisata : AppCompatActivity(), OnMapReadyCallback {


    private var options = MarkerOptions()
    var arrayList_Marker = ArrayList<LatLng>()
    var arrayList_NamaWisata = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tersimpan_wisata)

        val databaseHandler: Sqlite_DatabaseHandler = Sqlite_DatabaseHandler(this)
        //calling the viewEmployee method of DatabaseHandler class to read the records
        val emp: List<Sqlite_Model> = databaseHandler.viewEmployee()
        val ArrayNama = Array<String>(emp.size){"null"}
        val ArrayLat = Array<String>(emp.size){"null"}
        val ArrayLon = Array<String>(emp.size){"null"}
        var index = 0
        for(e in emp){
            arrayList_Marker.add(LatLng(e.lat.toDouble(), e.lon.toDouble()))
            ArrayNama[index] = e.nama
            arrayList_NamaWisata.add(ArrayNama[index])
            index++
        }
        val mapFragment : SupportMapFragment = supportFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        showWisataTersimpan()

        btnback_tersimpan.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
        }
    }

    fun showWisataTersimpan(){
        //creating the instance of DatabaseHandler class
        val databaseHandler: Sqlite_DatabaseHandler = Sqlite_DatabaseHandler(this)
        //calling the viewEmployee method of DatabaseHandler class to read the records
        val emp: List<Sqlite_Model> = databaseHandler.viewEmployee()
        val ArrayNama = Array<String>(emp.size){"null"}
        val ArrayLat = Array<String>(emp.size){"null"}
        val ArrayLon = Array<String>(emp.size){"null"}
        var index = 0
        for(e in emp){
            ArrayNama[index] = e.nama
            ArrayLat[index] = e.lat
            ArrayLon[index] = e.lon
            index++
        }
        //creating custom ArrayAdapter
        val myListAdapter = Adapter_ListTersimpan(this, ArrayNama, ArrayLat, ArrayLon)
        listView.adapter = myListAdapter
    }

    fun tampungWisataSementara(){

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        val map = googleMap
        val zoomLevel = 11f
        var index=0
        for (point in arrayList_Marker) {
            options.position(point)
            options.title(""+arrayList_NamaWisata[index])
            /*options.snippet("")
            */
            googleMap!!.addMarker(options)
            index++
        }
        val x = arrayList_Marker[0]

        if (map != null) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(x, zoomLevel))
            map.addMarker(MarkerOptions().position(x))
        }
    }
}