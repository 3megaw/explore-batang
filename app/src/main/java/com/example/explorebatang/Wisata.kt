package com.example.explorebatang

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.Data.Data_Wisata
import com.example.explorebatang.Holder.Holder_Wisata
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.flaviofaria.kenburnsview.KenBurnsView
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail__info_wisata.*
import kotlinx.android.synthetic.main.activity_wisata.*
import kotlinx.android.synthetic.main.rec_wisata.*

class Wisata : AppCompatActivity() {

    //    lateinit var adapterwisata: Adapter_Wisata
    lateinit var recyclerView : RecyclerView
    lateinit var adapter2: FirebaseRecyclerAdapter<Data_Wisata, Holder_Wisata>

    var tampung_lat:String? = null
    var tampung_long:String? = null
    var tampung_jenis:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        var bg: KenBurnsView? = null

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wisata)
        bg = findViewById(R.id.img_bgwisata)
        bg.resume()

        val i = this.intent
        tampung_lat = i.extras?.getString("latgps")
        tampung_long = i.extras?.getString("longgps")
        tampung_jenis = i.extras?.getString("jenis")
        cek_gps.setText(tampung_lat+" - "+tampung_long)
        LoadData_Wisata()

        //recyclerView.findViewHolderForAdapterPosition(0)?.itemView?.performClick()

        imgbtn_backwisata.setOnClickListener {
            finish()
        }

    }

    fun LoadData_Wisata(){
        recyclerView = findViewById(R.id.recycler_wisata)
        recyclerView.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        val options = FirebaseRecyclerOptions.Builder<Data_Wisata>()
                .setQuery(
                    FirebaseDatabase.getInstance().getReference("CF_Wisata")
                        .orderByChild("jenis_wisata").equalTo(
                        "" + tampung_jenis
                    ),
                    Data_Wisata::class.java
                )
                .build()
        adapter2 = object : FirebaseRecyclerAdapter<Data_Wisata, Holder_Wisata>(options) {
            override fun onBindViewHolder(
                holder: Holder_Wisata,
                position: Int,
                model: Data_Wisata
            ) {

                holder.judul_wisata.setText(model.nama_wisata)
                holder.lokasi_wisata.setText(model.lokasi_wisata)
                if(model.image_wisata?.isEmpty() == true){
                    holder.image_wisata.setImageResource(R.drawable.circlered)
                }else{
                    Picasso.get().load(model.image_wisata).into(holder.image_wisata)
                }
                if(position==0){
                    txt_namawisata.setText(model.nama_wisata)
                    txt_lokasiwisata.setText(model.lokasi_wisata)
                    if(model.image_wisata?.isEmpty() == true){
                        holder.image_wisata.setImageResource(R.drawable.circlered)
                    }else{
                        Picasso.get().load(model.image_wisata).into(img_bgwisata)
                    }
                    txt_deskripsisingkatinfowisata.setText(model.deskripsi)
                    txt_tiketwisata.setText(model.tiket)
                    button_detailwisata.setOnClickListener {
                        if (tampung_lat == null && tampung_long == null) {
                            val bundle = Bundle()
                            bundle.putString("namawisata", holder.judul_wisata.text.toString())
                            bundle.putString("deskripsi", model.deskripsi)
                            bundle.putString("fasilitas", model.fasilitas)
                            bundle.putString("gambar", model.image_wisata)
                            bundle.putString("jenis", model.jenis_wisata)
                            bundle.putString("ketinggian", model.ketinggian)
//                        bundle.putString("kode",)
//                        bundle.putString("lokasi",t_lokasiwisata)
                            bundle.putString("lat", model.lat_wisata)
                            bundle.putString("long", model.long_wisata)
                            bundle.putString("wakturamai", model.wakturamai)
                            /* bundle.putString("tiket",)
                            bundle.putString("tiketdetail",)*/

                            val intent = Intent(
                                this@Wisata,
                                Wisata_Detail::class.java
                            ) //THIS BISA PAKAI CONTEXT KALAU DI BEDA CLASS
                            intent.putExtras(bundle)
                            startActivity(intent)
                        } else {
                            val bundle = Bundle()
                            bundle.putString("namawisata", holder.judul_wisata.text.toString())
                            bundle.putString("deskripsi", model.deskripsi)
                            bundle.putString("fasilitas", model.fasilitas)
                            bundle.putString("gambar", model.image_wisata)
                            bundle.putString("jenis", model.jenis_wisata)
                            bundle.putString("ketinggian", model.ketinggian)
//                        bundle.putString("kode",)
//                        bundle.putString("lokasi",t_lokasiwisata)
                            bundle.putString("lat", model.lat_wisata)
                            bundle.putString("long", model.long_wisata)
                            bundle.putString("gpslat", tampung_lat)
                            bundle.putString("gpslong", tampung_long)
                            bundle.putString("wakturamai", model.wakturamai)
                            /* bundle.putString("tiket",)
                            bundle.putString("tiketdetail",)*/

                            val intent = Intent(
                                this@Wisata,
                                Wisata_Detail::class.java
                            ) //THIS BISA PAKAI CONTEXT KALAU DI BEDA CLASS
                            intent.putExtras(bundle)
                            startActivity(intent)
                        }
                    }
                }

                holder.cardview_wisata.setOnClickListener(View.OnClickListener {
                    txt_namawisata.setText(model.nama_wisata)
                    txt_lokasiwisata.setText(model.lokasi_wisata)
                    if(model.image_wisata?.isEmpty() == true){
                        holder.image_wisata.setImageResource(R.drawable.circlered)
                    }else{
                        Picasso.get().load(model.image_wisata).into(img_bgwisata)
                    }
                    txt_deskripsisingkatinfowisata.setText(model.deskripsi)
                    txt_tiketwisata.setText(model.tiket)

                    button_detailwisata.setOnClickListener {
                        if (tampung_lat == null && tampung_long == null) {
                            val bundle = Bundle()
                            bundle.putString("namawisata", holder.judul_wisata.text.toString())
                            bundle.putString("deskripsi", model.deskripsi)
                            bundle.putString("fasilitas", model.fasilitas)
                            bundle.putString("gambar", model.image_wisata)
                            bundle.putString("jenis", model.jenis_wisata)
                            bundle.putString("ketinggian", model.ketinggian)
//                        bundle.putString("kode",)
//                        bundle.putString("lokasi",t_lokasiwisata)
                            bundle.putString("lat", model.lat_wisata)
                            bundle.putString("long", model.long_wisata)
                            bundle.putString("wakturamai", model.wakturamai)
                            /* bundle.putString("tiket",)
                            bundle.putString("tiketdetail",)*/

                            val intent = Intent(
                                this@Wisata,
                                Wisata_Detail::class.java
                            ) //THIS BISA PAKAI CONTEXT KALAU DI BEDA CLASS
                            intent.putExtras(bundle)
                            startActivity(intent)
                        } else {
                            val bundle = Bundle()
                            bundle.putString("namawisata", holder.judul_wisata.text.toString())
                            bundle.putString("deskripsi", model.deskripsi)
                            bundle.putString("fasilitas", model.fasilitas)
                            bundle.putString("gambar", model.image_wisata)
                            bundle.putString("jenis", model.jenis_wisata)
                            bundle.putString("ketinggian", model.ketinggian)
//                        bundle.putString("kode",)
//                        bundle.putString("lokasi",t_lokasiwisata)
                            bundle.putString("lat", model.lat_wisata)
                            bundle.putString("long", model.long_wisata)
                            bundle.putString("gpslat", tampung_lat)
                            bundle.putString("gpslong", tampung_long)
                            bundle.putString("wakturamai", model.wakturamai)
                            /* bundle.putString("tiket",)
                            bundle.putString("tiketdetail",)*/

                            val intent = Intent(
                                this@Wisata,
                                Wisata_Detail::class.java
                            ) //THIS BISA PAKAI CONTEXT KALAU DI BEDA CLASS
                            intent.putExtras(bundle)
                            startActivity(intent)
                        }
                    }
                })

            }


            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder_Wisata {
                val v = LayoutInflater.from(parent.context).inflate(
                    R.layout.rec_wisata,
                    parent,
                    false
                )
                return Holder_Wisata(v)
            }
        }
        adapter2.startListening()
        recyclerView.setAdapter(adapter2)
    }

}