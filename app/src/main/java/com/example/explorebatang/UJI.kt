package com.example.explorebatang

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.Data.DataModel_Rule
import com.example.explorebatang.Data.Data_DayaTarik
import com.example.explorebatang.Holder.Holder_AdmDayaTarik
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_adm__nilai_daya_tarik_edit.*
import kotlinx.android.synthetic.main.activity_u_j_i.*


class UJI : AppCompatActivity() {

    lateinit var adapter2: FirebaseRecyclerAdapter<DataModel_Rule, Holder_AdmDayaTarik>

    private var dbRuleReference: DatabaseReference? = null
    private var dbDTReference:DatabaseReference?=null
    private lateinit var db: FirebaseDatabase

    private var list_kodedayatarikrule = arrayListOf<DataModel_Rule>()
    var tampungkode2:String? = "W2"
    var tampungkode3:String? = ""
    //private var adapter: UJI_Adapter? = null

    var adapter: RecyclerView.Adapter<*>? = null
    var recyclerViewAdapter: RecyclerView.Adapter<*>? = null
    var recylerViewLayoutManager: RecyclerView.LayoutManager? = null

    var list:List<String>? = null
    var context: Context? = null
    private var listData: List<DataModel_Rule>? = null
    private var listData2: List<Data_DayaTarik>? = null

    val listkode2 = ArrayList<String>()
    val listkode: MutableList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_u_j_i)
        context = getApplicationContext()

        db = FirebaseDatabase.getInstance()
        dbRuleReference = db.getReference("CF_Rule")
        dbDTReference = db.getReference("CF_DayaTarik")
        val queryRule = dbRuleReference!!.orderByChild("kode_wisata").equalTo(tampungkode2)
        val queryRule2 = dbDTReference!!.orderByChild("kode_dayatarik")

        listData = ArrayList()
        listData2 = ArrayList()

        var recylerViewLayoutManager: RecyclerView.LayoutManager
        val subjects = arrayOf(
                "ANDROID",
                "PHP",
                "BLOGGER",
                "WORDPRESS",
                "JOOMLA",
                "ASP.NET",
                "JAVA",
                "C++",
                "MATHS",
                "HINDI",
                "ENGLISH")

        queryRule.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (npsnapshot in dataSnapshot.children) {
                        queryRule2.addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot2: DataSnapshot) {
                                val l: DataModel_Rule = npsnapshot.getValue(DataModel_Rule::class.java) as DataModel_Rule
                                val ll: String? = npsnapshot.child("kode_dayatarik").toString()
                                for (npsnaphot2 in dataSnapshot2.children) {
                                    val d: Data_DayaTarik = npsnaphot2.getValue(Data_DayaTarik::class.java) as Data_DayaTarik
                                    val dd: String? = npsnaphot2.child("kode_dayatarik").toString()
                                    if (ll.equals(dd)) {
                                        val tampungnamadaya: String? = npsnaphot2.child("nama_dayatarik").value.toString()
                                        Toast.makeText(this@UJI, tampungnamadaya, Toast.LENGTH_LONG).show()
                                        if (ll != null) {
                                            listkode2.add(tampungnamadaya.toString())
                                            (listData as ArrayList<DataModel_Rule>).add(l)
                                            (listData2 as ArrayList<Data_DayaTarik>).add(d)
                                        }
                                    }
                                }
                            }

                            override fun onCancelled(error: DatabaseError) {
                                // Failed to read value
                                //Log.w(TAG, "Failed to read value.", error.toException())
                            }
                        })
                    }
//                    adapter = UJI_Adapter(listData as ArrayList<DataModel_Rule>, listData2 as ArrayList<Data_DayaTarik>)
              //      recyclerViewAdapter = RecyclerViewAdapter(context, subjects)
//                    adapter!!.notifyDataSetChanged()
                    val llm = LinearLayoutManager(this@UJI)
                    llm.orientation = LinearLayoutManager.VERTICAL
                    rec_UJI.setLayoutManager(llm)
                    rec_UJI.setAdapter(recyclerViewAdapter)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })



    }

}

