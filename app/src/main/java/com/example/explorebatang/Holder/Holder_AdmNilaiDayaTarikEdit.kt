package com.example.explorebatang.Holder

import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.R

class Holder_AdmNilaiDayaTarikEdit(view : View) : RecyclerView.ViewHolder(view)  {
    var kode:TextView = itemView.findViewById(R.id.txt_admrec_kodedayatarikedit)
    var namadaya:TextView = itemView.findViewById(R.id.txt_admrec_dayatarikedit)
    var carddaya:CardView = itemView.findViewById(R.id.card_listdayatarikedit)
}