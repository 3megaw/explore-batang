package com.example.explorebatang.Holder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.R

class Holder_Wisata(view : View) : RecyclerView.ViewHolder(view) {
    var judul_wisata : TextView = itemView.findViewById(R.id.txt_recwisata)
    var lokasi_wisata : TextView = itemView.findViewById(R.id.txt_reclokasiwisata)
    var image_wisata : ImageView = itemView.findViewById(R.id.img_recbgwisata)
    var image_gradwisata : ImageView = itemView.findViewById(R.id.img_recgradwisata)
    var cardview_wisata : CardView = itemView.findViewById(R.id.cardview_wisata)
}