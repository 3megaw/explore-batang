package com.example.explorebatang.Holder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.R

class Holder_JenisWisata(view:View) : RecyclerView.ViewHolder(view) {
    var namajeniswisata : TextView = itemView.findViewById(R.id.txt_recjeniswisata)
    var imagejeniswisata: ImageView = itemView.findViewById(R.id.img_recjenisbgwisata)
    var cardjeniswisata: CardView = itemView.findViewById(R.id.cardview_jeniswisata)

}