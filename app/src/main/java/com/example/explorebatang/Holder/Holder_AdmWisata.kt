package com.example.explorebatang.Holder

import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.R

class Holder_AdmWisata(view : View) : RecyclerView.ViewHolder(view) {
    var nama_wisata : TextView = itemView.findViewById(R.id.txt_admrecwisata)
    var lokasi_wisata : TextView = itemView.findViewById(R.id.txt_admreclokasiwisata)
    var listwisata: CardView = itemView.findViewById(R.id.card_listwisata)
}