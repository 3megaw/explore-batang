package com.example.explorebatang.Holder

import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.R

class Holder_AdmKomentar(view : View) : RecyclerView.ViewHolder(view) {
    var komentar:TextView = itemView.findViewById(R.id.txt_admrec_komentar)
    var listkomentar: CardView = itemView.findViewById(R.id.card_listkomentar)
    var tanggalkomentar: TextView = itemView.findViewById(R.id.txt_admrec_tanggalkomentar)
    var wisatakomentar: TextView = itemView.findViewById(R.id.txt_admrec_wisata)
}