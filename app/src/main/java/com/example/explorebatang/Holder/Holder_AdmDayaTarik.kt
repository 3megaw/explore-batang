package com.example.explorebatang.Holder

import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.R

class Holder_AdmDayaTarik(view: View):RecyclerView.ViewHolder(view) {
    var nama_dayatarik : TextView = itemView.findViewById(R.id.txt_admrec_dayatarik)
    var kode_dayatarik : TextView = itemView.findViewById(R.id.txt_admrec_kodedayatarik)
    var list_dayatarik: CardView = itemView.findViewById(R.id.card_listdayatarik)
}