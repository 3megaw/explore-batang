package com.example.explorebatang

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_u_j_i2.*


class RecyclerViewAdapter(
        var SubjectValues2: ArrayList<String>,
        var SubjectValues: ArrayList<String>,
        var SubjectValuesIDR: ArrayList<String>,
        var SubjectValuesKodeWisata: String,
        var context: Context
) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    var view1: View? = null
    var viewHolder1: ViewHolder? = null
    var textView: TextView? = null
    fun RecyclerViewAdapter(SubjectValues2: ArrayList<String>, SubjectValues: ArrayList<String>, SubjectValuesIDR: ArrayList<String>, SubjectValuesKodeWisata: String, context: Context?) {
        this.SubjectValues = SubjectValues
        this.SubjectValues2 = SubjectValues2
        this.SubjectValuesIDR = SubjectValuesIDR
        this.SubjectValuesKodeWisata = SubjectValuesKodeWisata
        this.context = context!!
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       // holder.textView2.text = SubjectValues2[position]
        holder.textView.text = SubjectValues[position]
        holder.textView.setOnClickListener {
            val tampung:String = SubjectValues2[position]
            val tampung2:String= SubjectValuesIDR[position]
            //Toast.makeText(context, SubjectValues2[position], Toast.LENGTH_LONG).show()

            val bundle = Bundle()
            bundle.putString("tampung_kodedayatarik", SubjectValues2[position])
            bundle.putString("tampung_koderule", SubjectValuesIDR[position])
            bundle.putString("tampung_kodewisataNext", SubjectValuesKodeWisata)

            val intent = Intent(context, Adm_NilaiDayaTarikEdit::class.java)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
    }

    class ViewHolder(v: View?) : RecyclerView.ViewHolder(v!!) {
        var textView: TextView
        //var textView2: TextView
        init {
            textView = v!!.findViewById<View>(com.example.explorebatang.R.id.txt_1) as TextView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        view1 = LayoutInflater.from(context).inflate(
                com.example.explorebatang.R.layout.card_adm_nilaidayatarikedit,
                parent,
                false
        )
        viewHolder1 = ViewHolder(view1)
        return viewHolder1!!
    }


    override fun getItemCount(): Int {
        return SubjectValues.size
    }
}