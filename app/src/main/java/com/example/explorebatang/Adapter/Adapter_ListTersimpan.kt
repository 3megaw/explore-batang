package com.example.explorebatang.adapter

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.explorebatang.R

class Adapter_ListTersimpan(private val context: Activity, private val name: Array<String>, private val lat: Array<String>, private val lon: Array<String>)
    : ArrayAdapter<String>(context, R.layout.list_tersimpan, name) {
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.list_tersimpan, null, true)

        val namaWisata = rowView.findViewById(R.id.textViewId) as TextView
//        val lonWisata = rowView.findViewById(R.id.textViewEmail) as TextView

        namaWisata.text = "${name[position]}"
//        lonWisata.text = "Email: ${lon[position]}"
        return rowView
    }
}