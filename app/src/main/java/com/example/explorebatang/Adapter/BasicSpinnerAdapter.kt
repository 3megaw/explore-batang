package com.example.explorebatang.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.explorebatang.R
import java.util.*

class BasicSpinnerAdapter(
    private val dayaTarikData: ArrayList<String>,
    private val context: Context
) : BaseAdapter() {
    val listSelectedPossibilites = arrayListOf<String>()
    private val possibilityName = listOf("Mungkin", "Hampir Pasti", "Pasti")

    init {
        for (i in dayaTarikData.indices){
            listSelectedPossibilites.add("Mungkin")
        }
    }

    override fun getCount(): Int {
        return dayaTarikData.size
    }

    override fun getItem(position: Int): Any {
        return dayaTarikData[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    @SuppressLint("InflateParams")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view = convertView
            ?: (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                R.layout.list_dayatarik_cfuser,
                null
            )

        (view.findViewById<View>(R.id.row_item_textview) as TextView).also {
            it.text = dayaTarikData[position]
        }

        (view.findViewById<View>(R.id.row_item_spinner) as Spinner).also {
            it.adapter = ArrayAdapter(context, R.layout.spinner_nilaidayatarik, possibilityName)
            it.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(adapterView: AdapterView<*>?, p1: View?, index: Int, p3: Long) {
                    listSelectedPossibilites[position] = adapterView?.getItemAtPosition(index)?.toString()!!
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }

            }
        }

        return view
    }

}