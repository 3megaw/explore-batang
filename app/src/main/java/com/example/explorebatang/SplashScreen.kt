package com.example.explorebatang

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_splash_screen.*


class SplashScreen : AppCompatActivity() {

    // This is the loading time of the splash screen
    private val SPLASH_TIME_OUT:Long = 3000 // 1 sec

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)


        try {

            val videoHolder = VideoView(this)
            setContentView(videoHolder)
            val video = Uri.parse("android.resource://" + packageName + "/" + R.raw.splash)
            videoHolder.setVideoURI(video)

            videoHolder.setOnCompletionListener { jump() }

            videoHolder.start()
        } catch (ex: Exception) {
            jump()
        }

    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        jump()
        return true}

    private fun jump() {
        if (isFinishing)
            return

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)

        finish()


        /*val animSlide = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide)
        val animSlide2 = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide2)
        awan1.startAnimation(animSlide)
        awan2.startAnimation(animSlide2)
        awan3.startAnimation(animSlide)
        Handler().postDelayed({
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }, SPLASH_TIME_OUT)*/
    }

}