package com.example.explorebatang

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.explorebatang.Data.Data_DayaTarik2
import com.example.explorebatang.PendukungKeputusan_NilaiUser.Companion.EXTRA_HASIL
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_pendukung_keputusan.*
import kotlinx.android.synthetic.main.activity_side_menu.*
import java.lang.String
import java.util.*
import kotlin.collections.ArrayList


class PendukungKeputusan_DayaTarik : AppCompatActivity() {
    private var dataAdapter: MyCustomAdapter? = null
    var databaseReference: DatabaseReference? = null

    lateinit var viewFlipper: ViewFlipper
    var gejalaList: ArrayList<Data_DayaTarik2?> = ArrayList<Data_DayaTarik2?>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pendukung_keputusan)

        btn_back_keputusandayatarik.setOnClickListener {
            finish()
        }

        vflip.setInAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left))
        vflip.setOutAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right))
        vflip.flipInterval = 8000
        vflip.isAutoStart = true
        vflip2.setTypeface(null, Typeface.ITALIC)
        vflip3.setTypeface(null, Typeface.ITALIC)

        databaseReference = FirebaseDatabase.getInstance().getReference("CF_DayaTarik")
        databaseReference!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                gejalaList.clear()
                for (postSnapshot in snapshot.children) {
                    val gejala: Data_DayaTarik2? = postSnapshot.getValue(Data_DayaTarik2::class.java)
                    gejalaList.add(gejala)
                }
                Collections.sort(gejalaList) { s1, s2 -> String.valueOf(s1!!.nama_dayatarik).compareTo(s2!!.nama_dayatarik) }
                dataAdapter = MyCustomAdapter(this@PendukungKeputusan_DayaTarik, R.layout.list_dayatarik, gejalaList)
                val listView = findViewById<ListView>(R.id.list_dayatarik)
                listView.adapter = dataAdapter // set listview dengan dataadapter
                // dataAdapter!!.notifyDataSetChanged();
                search_spk.setOnQueryTextListener(object :SearchView.OnQueryTextListener{
                    override fun onQueryTextSubmit(query: kotlin.String?): Boolean {
                        search_spk.clearFocus()
                        if(gejalaList.contains(query.toString())){
                            dataAdapter!!.filter.filter(query)
                        }else{
                            Toast.makeText(applicationContext,"Maaf daya tarik yang anda cari belum ada",Toast.LENGTH_SHORT).show()
                        }
                        return false
                    }
                    override fun onQueryTextChange(newText: kotlin.String?): Boolean {
                        dataAdapter!!.filter.filter(newText)
                        return false
                    }
                })
            }

            override fun onCancelled(error: DatabaseError) {}
        })


        /*search_spk.setOnQueryTextListener(object :SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(s: kotlin.String): Boolean {
                //searchview_spk.clearFocus()
                search(s)
                return false
            }
            override fun onQueryTextChange(s: kotlin.String): Boolean {
                search(s)
                return false
            }
        })*/
        btn_hasilspk.setOnClickListener { v ->
            val gejalaTerpilih = StringBuffer()
            //                array gejala yang diceklis
            val gejalaList: ArrayList<Data_DayaTarik2?> = dataAdapter!!.dayatarikList
            for (i in gejalaList.indices) {
                val dayatarik: Data_DayaTarik2? = gejalaList[i]
                if (dayatarik!!.getSelected()) { // cek apakah gejala diceklis
                    gejalaTerpilih.append(dayatarik?.nama_dayatarik).append("#")
                    //   gejalaTerpilih.append(gejala.getKode_gejala()).append("#");
                }
            }
            // jika tidak ada yang diceklis
            if (gejalaTerpilih.toString() == "") {
                Toast.makeText(this, "Silakan pilih daya tarik!", Toast.LENGTH_SHORT).show()
            } else {
                // tampilkan activity hasil diagnosa
                val myIntent = Intent(v.context, PendukungKeputusan_NilaiUser::class.java)
                myIntent.putExtra(EXTRA_HASIL, gejalaTerpilih.toString())
                startActivity(myIntent)
            }
        }
    }

    private fun search(s: kotlin.String){
        var query: Query = databaseReference!!.child("CF_DayaTarik").orderByChild("nama_dayatarik")
                .startAt(s)
                .endAt(s.toString() + "\uf8ff")
        query!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                gejalaList.clear()
                for (postSnapshot in snapshot.children) {
                    val gejala: Data_DayaTarik2? = postSnapshot.getValue(Data_DayaTarik2::class.java)
                    gejalaList.add(gejala)
                }
                /*Collections.sort(gejalaList) { s1, s2 -> String.valueOf(s1!!.nama_dayatarik).compareTo(s2!!.nama_dayatarik) }
                dataAdapter = MyCustomAdapter(this@PendukungKeputusan_DayaTarik, R.layout.list_dayatarik, gejalaList)
                val listView = findViewById<ListView>(R.id.list_dayatarik)
                listView.adapter = dataAdapter // set listview dengan dataadapter
                */
                dataAdapter!!.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }

    private class MyCustomAdapter internal constructor(
            context: Context?, textViewResourceId: Int,
            dayatarikList: ArrayList<Data_DayaTarik2?>?
    ) :
        ArrayAdapter<Data_DayaTarik2?>(context!!, textViewResourceId, dayatarikList!!) {
        internal val dayatarikList: ArrayList<Data_DayaTarik2?>

        private inner class ViewHolder {
            var name: CheckBox? = null
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView
            var holder: ViewHolder? = null
            if (convertView == null) {
                val vi = context.getSystemService(
                        LAYOUT_INFLATER_SERVICE
                ) as LayoutInflater
                convertView = vi.inflate(R.layout.list_dayatarik, null)
                holder = ViewHolder()
                holder!!.name = convertView!!.findViewById<CheckBox>(R.id.cb_dayatarik)
                convertView.setTag(holder)
                holder.name?.setOnClickListener(View.OnClickListener { v ->
                    val cb = v as CheckBox
                    val gejala: Data_DayaTarik2 = cb.tag as Data_DayaTarik2
                    gejala.setSelected(cb.isChecked)
                })
            } else {
                holder = convertView.tag as ViewHolder
            }
            val dayatarik: Data_DayaTarik2? = dayatarikList[position]
            holder.name!!.setText(dayatarik!!.nama_dayatarik)
            holder.name!!.isChecked = dayatarik.getSelected()
            holder.name!!.tag = dayatarik
            return convertView!!
        }
        init {
            this.dayatarikList = ArrayList<Data_DayaTarik2?>()
            this.dayatarikList.addAll(dayatarikList!!)
        }
    }
}