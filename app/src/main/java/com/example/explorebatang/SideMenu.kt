package com.example.explorebatang

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_side_menu.*

class SideMenu : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_side_menu)

        auth = FirebaseAuth.getInstance()
        if(auth.currentUser!=null){
            btn_masuk.visibility = View.GONE
            linear_daftar.visibility = View.GONE
        }else{
            btn_keluar.visibility = View.GONE
        }
//        txt_beranda.setText(auth.currentUser!!.uid)

        buttonSideMenu()
    }



    private fun buttonSideMenu(){
        btn_back.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right)
        }
        tersimpan.setOnClickListener {
            val intent = Intent(this,TersimpanWisata::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left)
        }
        faq.setOnClickListener {
            val intent = Intent(this,com.example.explorebatang.faq::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left)
        }
        tentang.setOnClickListener {
            val intent = Intent(this,Tentang::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left)
        }

        txt_daftar.setOnClickListener {
            val intent = Intent(this,Daftar::class.java)
            startActivity(intent)
            finish()
        }
        btn_masuk.setOnClickListener {
            val intent = Intent(this,Login::class.java)
            startActivity(intent)
            finish()
        }
        btn_keluar.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}