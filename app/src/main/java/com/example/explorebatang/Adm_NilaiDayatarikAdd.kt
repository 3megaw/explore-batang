package com.example.explorebatang

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_adm__dayatarik_add.*
import kotlinx.android.synthetic.main.activity_adm__nilai_daya_tarik_edit.*
import kotlinx.android.synthetic.main.activity_adm__nilai_dayatarik_add.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set


class Adm_NilaiDayatarikAdd : AppCompatActivity() {

    private var tampungkey : Int? = null
    private var keyString : String? = null

    private val arrayList: ArrayList<String> = ArrayList()
    private val arrayList2: ArrayList<String> = ArrayList()
    private val arrayList22: ArrayList<String> = ArrayList()

    private var tampung_dayatarik : String? = null
    private var tampung_wisata: String? = null
    private var tampung_kodewisata2:String? = null
    private var tampung_nilai:String? = null

    var listener: ValueEventListener? = null
    var list: ArrayList<String>? = null
    var list2: ArrayList<String>? = null
    private var list22: ArrayList<String>? = null

    var adapter: ArrayAdapter<String>? = null
    var adapter2: ArrayAdapter<String>? = null
    var dialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm__nilai_dayatarik_add)

        list = ArrayList()
        list2 = ArrayList()
        list22 = ArrayList()

        adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, list!!)
        adapter?.setDropDownViewResource(R.layout.spinner_nilaidayatarik)
        spin_addnilaidayatarik_namawisata.setAdapter(adapter)
        spin_addnilaidayatarik_namawisata.setOnItemSelectedListener(object :
            OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                (parent.getChildAt(0) as TextView).setTextColor(Color.WHITE) /* if you want your item to be white */
                tampung_wisata = parent.getItemAtPosition(position).toString()
                getKodeWisata()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })


        getLastKode()
        showDataSpinner()
        showDataDialog()

        btn_addnilaidayatarik_unggah.setOnClickListener {
            unggahNilaiDayatarik()
        }
        btnback_pengetahuanadd.setOnClickListener {
            finish()
        }
    }


    private fun getKodeWisata(){
        var dbref = FirebaseDatabase.getInstance().getReference("CF_Wisata")
        var lastquery: Query = dbref.orderByChild("nama_wisata").equalTo(tampung_wisata)
        lastquery.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (data in dataSnapshot.children) {
                    tampung_kodewisata2 = data.child("kode_wisata").getValue(String::class.java)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun getLastKode(){
        var dbref = FirebaseDatabase.getInstance().getReference().child("CF_Rule")
        var lastquery: Query = dbref.orderByKey().limitToLast(1)
        lastquery.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (data in dataSnapshot.children) {
                    tampungkey = data.key?.toInt()
                }
                tampungkey = tampungkey?.plus(1)
                keyString = tampungkey.toString()
                edt_addnilaidayatarik_koderule.setText(keyString)
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun unggahNilaiDayatarik(){

        val mDatabase = keyString?.let {
            FirebaseDatabase.getInstance().getReference("CF_Rule").child(
                it
            )
        }
        tampung_nilai = edt_adddnilaiayatarik_nilai.getText().toString().trim()
        mDatabase?.child("id_rule")?.setValue(keyString?.toDouble())
        mDatabase?.child("kode_dayatarik")?.setValue(tampung_dayatarik.toString().trim())
        mDatabase?.child("kode_wisata")?.setValue(tampung_kodewisata2)
        mDatabase?.child("nilai_cf")?.setValue(tampung_nilai)

        getLastKode()
        clearEDT()
    }

    private fun clearEDT(){
        edt_adddnilaiayatarik_nilai.setText("")
    }

    private fun showDataSpinner(){
        var dbref = FirebaseDatabase.getInstance().getReference().child("CF_Wisata")
        //  var lastquery: Query = dbref.orderByKey().limitToLast(1)
        dbref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                arrayList.clear()
                for (data in dataSnapshot.children) {
                    list?.add(data.child("nama_wisata").getValue().toString())
                }
                adapter?.notifyDataSetChanged()

            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun showDataDialog(){
        var dbref = FirebaseDatabase.getInstance().getReference().child("CF_DayaTarik")
        dbref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                arrayList2.clear()
                for (data in dataSnapshot.children) {
                    list2?.add(data.child("nama_dayatarik").getValue().toString())
                    list22?.add(data.child("kode_dayatarik").getValue().toString())
                }
                val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                    applicationContext,
                    android.R.layout.simple_spinner_item,
                    list2!!
                )
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                searchableSpinner.setAdapter(adapter)
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })

        searchableSpinner.setOnItemSelectedListener(object :
            OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                (parent.getChildAt(0) as TextView).setTextColor(Color.WHITE) /* if you want your item to be white */
                //tampung_dayatarik = parent.getItemAtPosition(position).toString()
                tampung_dayatarik = list22?.get(position).toString()
                Toast.makeText(parent.getContext(), tampung_dayatarik, Toast.LENGTH_SHORT).show();
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
    }
}