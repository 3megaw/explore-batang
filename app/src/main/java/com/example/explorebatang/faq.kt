package com.example.explorebatang

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import com.example.explorebatang.Adapter.CustomExpandableListAdapter
import kotlinx.android.synthetic.main.activity_faq.*

class faq : AppCompatActivity() {
    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String> ? = null

    val data: HashMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val spk = ArrayList<String>()
            spk.add("Sistem pendukung keputusan merupakan sebuah sistem yang dapat mengolah informasi yang dimasukan oleh pengguna untuk dijadikan suatu keputusan berdasarkan aturan tertentu")

            val penggunaanspk = ArrayList<String>()
            penggunaanspk.add("Pengguna hanya perlu memilih daya tarik yang diinginkan dan juga dapat mengisi pembobotan berdasarakan dari yakin, kurang yakin, mungkin. Setelah itu sistem akan secara otomatis mencari keputusan berdasarkan masukan pengguna")

            val bobotspk = ArrayList<String>()
            bobotspk.add("Pembobotan secara default sistem akan mengisi dengan nilai mungkin, namun pengguna juga dapat memilih sendiri berdasarkan kriteria keinginannya")

            val komentar = ArrayList<String>()
            komentar.add("Untuk mengirimkan komentar, pengguna diwajibkan masuk ke akun pengguna yang sudah terdaftar terlebih dahulu. Apabila pengguna belum terdaftar maka pengguna dapat mendaftar dengan mengisi email dan kata sandi")

            val jarak = "Penggunaan jarak wisata pada aplikasi ini dengan menggunakan GPS dari perangkat pengguna, apabila jarak tidak muncul maka kemungkinan besar pengguna belum mengaktifkan GPS atau juga belum mengizinkan aplikasi ini menggunakan GPS pada ponsel pengguna. Untuk mengizinkan aplikasi dapat mengakses GPS maka pengguna dapat menuju ke Pengaturan -> Aplikasi -> Izin (Mungkin dapat berbeda setiap perangkat)"
            val ramai = "Ramai atau tidaknya tempat wisata telah ditentukan berdasarkan data pada sistem dari narasumber, ramai atau tidaknya tempat wisata didapat berdasarkan waktu kebiasaan pengunjung mengunjungi tempat tersebut"

            listData["Apa itu sistem pendukung keputusan"] = spk
            listData["Bagaimana menggunakan sistem pendukung keputusan"] = penggunaanspk
            listData["Apakah pembobotan wajib diganti"] = bobotspk
            listData["Bagaimana mengirimkan komentar"] = komentar
            listData["Kenapa jarak wisata tidak muncul"] = listOf(jarak)
            listData["Bagaimana aplikasi mengetahui ramai atau tidaknya tempat wisata"] = listOf(ramai)

            return listData
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faq)

        btn_back_faq.setOnClickListener {
            finish()
        }

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)

            expandableListView!!.setOnGroupExpandListener { groupPosition -> Toast.makeText(applicationContext, (titleList as ArrayList<String>)[groupPosition] + " List Expanded.", Toast.LENGTH_SHORT).show() }

            expandableListView!!.setOnGroupCollapseListener { groupPosition -> Toast.makeText(applicationContext, (titleList as ArrayList<String>)[groupPosition] + " List Collapsed.", Toast.LENGTH_SHORT).show() }

            expandableListView!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
                Toast.makeText(applicationContext, "Clicked: " + (titleList as ArrayList<String>)[groupPosition] + " -> " + listData[(titleList as ArrayList<String>)[groupPosition]]!!.get(childPosition), Toast.LENGTH_SHORT).show()
                false
            }
        }
    }
}