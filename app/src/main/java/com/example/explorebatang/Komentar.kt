package com.example.explorebatang

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_adm__add_destinasi.*
import kotlinx.android.synthetic.main.activity_komentar.*
import java.text.SimpleDateFormat
import java.util.*


class Komentar : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private var tampungnamawisata: String? =null
    private var tampungkey:Int? = null
    private var keyString:String? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_komentar)

        getPassData()

        Toast.makeText(this,"", Toast.LENGTH_LONG).show()
        auth = FirebaseAuth.getInstance()
        if(auth.currentUser!=null){
            Toast.makeText(this,"Anda sudah login", Toast.LENGTH_LONG).show()
            Toast.makeText(this,""+auth.currentUser,Toast.LENGTH_LONG).show()
        }else{
            Toast.makeText(this,"Anda belum login", Toast.LENGTH_LONG).show()
            val intent = Intent(this, Login::class.java)
            startActivity(intent)
            finish()
        }
        btn_kirimkomentar.setOnClickListener {
            getLastKodeKomentar()
            kirimKomentar()
        }
    }

    private fun kirimKomentar(){
        val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy")
        val currentdate:String = simpleDateFormat.format(Date())
        val datastore = keyString?.let { it1 -> FirebaseDatabase.getInstance().getReference("Komentar").child(it1) }
        val hashMap = HashMap<String, String>()
        hashMap["idkomentar"] = keyString.toString()
        hashMap["iduser"] = auth.currentUser?.uid.toString()
        hashMap["komentar"] = edt_addkomentar.getText().toString().trim { it <= ' ' }
        hashMap["wisata"] = tampungnamawisata.toString()
        hashMap["tanggal"] = currentdate

        if (datastore != null) {
            datastore.setValue(hashMap).addOnSuccessListener {
                Toast.makeText(this, "Terkirim", Toast.LENGTH_SHORT).show()
                hashMap.clear()
            }
        }
    }


    fun getLastKodeKomentar(){
        var dbref = FirebaseDatabase.getInstance().getReference().child("Komentar")
        var lastquery: Query = dbref.orderByKey().limitToLast(1)
        lastquery.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (data in dataSnapshot.children) {
                    tampungkey = data.key?.toInt()
                }
                keyString = (tampungkey?.plus(1)).toString()
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    fun getPassData(){
        val x = this.intent
        tampungnamawisata=x.extras?.getString("namawisata")
        komentar_keteranganwisata.setText("Silahkan mengisi komentar, saran, atau kesan Anda mengenai tempat wisata $tampungnamawisata")
    }

}