package com.example.explorebatang

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.Data.DataModel_Rule
import com.example.explorebatang.Data.Data_DayaTarik
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_adm__dayatarikk_edit.*
import kotlinx.android.synthetic.main.activity_adm__nilai_daya_tarik_edit.*
import kotlinx.android.synthetic.main.activity_adm__nilai_dayatarik.*
import kotlinx.android.synthetic.main.activity_adm__nilai_dayatarik_add.*
import kotlinx.android.synthetic.main.activity_u_j_i2.*

class Adm_NilaiDayaTarikEdit : AppCompatActivity() {

    var context: Context? = null
    var recyclerViewAdapter: RecyclerView.Adapter<*>? = null
    var recylerViewLayoutManager: RecyclerView.LayoutManager? = null

    private var dbDTReference: DatabaseReference?=null
    private var dbRuleReference: DatabaseReference? = null
    private lateinit var db: FirebaseDatabase

    var tampungkode_wisata:String? = null
    var tampungkode_rule:String? = null
    var tampung_nilai:String?= null

    var adapter: RecyclerView.Adapter<*>? = null

    var list:List<String>? = null

    val listkode3 = ArrayList<String>()
    val listkode2 = ArrayList<String>()
    val listkode1 = ArrayList<String>()

    var adapterspin: ArrayAdapter<String>? = null
    var adapter2spin: ArrayAdapter<String>? = null
    var dialog: Dialog? = null
    private var tampung_wisata: String? = null
    private val arrayListSpin: ArrayList<String> = ArrayList()
    var listSpin: ArrayList<String>? = null
    var list2Spin: ArrayList<String>? = null
    private var list22Spin: ArrayList<String>? = null
    private val arrayList2Spin: ArrayList<String> = ArrayList()
    private var tampung_dayatarikSpin : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm__nilai_daya_tarik_edit)

        list = ArrayList()
        list2Spin = ArrayList()
        list22Spin = ArrayList()

        var tampungkode = this.intent
        tampungkode_wisata = tampungkode.extras?.getString("kodewisata").toString()

        val i = this.intent
        edt_editnilaidayatarik_koderule.setText(i.extras?.getString("tampung_koderule"))
        tampungkode_rule = i.extras?.getString("tampung_koderule")
        edt_editnilaidayatarik_kodewisata.setText(i.extras?.getString("tampung_kodewisataNext"))

        db = FirebaseDatabase.getInstance()
        dbRuleReference = db.getReference("CF_Rule")
        dbDTReference = db.getReference("CF_DayaTarik")
        val queryRule = dbRuleReference!!.orderByChild("kode_wisata").equalTo(tampungkode_wisata)
        val queryRule2 = dbDTReference!!.orderByChild("kode_dayatarik")

        var recylerViewLayoutManager: RecyclerView.LayoutManager

        queryRule.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (npsnapshot in dataSnapshot.children) {
                        queryRule2.addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot2: DataSnapshot) {
                                val l: DataModel_Rule = npsnapshot.getValue(DataModel_Rule::class.java) as DataModel_Rule
                                val ll: String? = npsnapshot.child("kode_dayatarik").toString()
                                val ll2: String? = npsnapshot.child("id_rule").value.toString()
                                listkode3.add(ll2.toString())
                                for (npsnaphot2 in dataSnapshot2.children) {
                                    val d: Data_DayaTarik = npsnaphot2.getValue(Data_DayaTarik::class.java) as Data_DayaTarik
                                    val dd: String? = npsnaphot2.child("kode_dayatarik").toString()
                                    if (ll.equals(dd)) {
                                        val tampungnamadaya: String? = npsnaphot2.child("nama_dayatarik").value.toString()
                                        val tampungkodedaya: String? = npsnapshot.child("kode_dayatarik").value.toString()
                                        if (ll != null) {
                                            listkode2.add(tampungnamadaya.toString())
                                            listkode1.add(tampungkodedaya.toString())
                                            val builder = StringBuilder()
                                            for (i in listkode2) {
                                                builder.append("$i ")
                                            }
                                            Toast.makeText(this@Adm_NilaiDayaTarikEdit, builder, Toast.LENGTH_LONG).show()
                                        }
                                    }
                                    recylerViewLayoutManager = LinearLayoutManager(context)
                                    rec_admpengetahuanedit!!.layoutManager = recylerViewLayoutManager
                                    recyclerViewAdapter = RecyclerViewAdapter(listkode1, listkode2, listkode3,
                                        tampungkode_wisata!!, this@Adm_NilaiDayaTarikEdit)
                                    rec_admpengetahuanedit!!.adapter = recyclerViewAdapter
                                }
                            }

                            override fun onCancelled(error: DatabaseError) {
                                // Failed to read value
                                //Log.w(TAG, "Failed to read value.", error.toException())
                            }
                        })
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })

        showDataSpinner()
        showDataDialog()
        Edit_NilaiDayaTarik()
        Delete_NilaiDayaTarik()
    }


    private fun showDataSpinner(){
        var dbref = FirebaseDatabase.getInstance().getReference().child("CF_DayaTarik")
        //  var lastquery: Query = dbref.orderByKey().limitToLast(1)
        dbref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                arrayListSpin.clear()
                for (data in dataSnapshot.children) {
                    listSpin?.add(data.child("nama_dayatarik").getValue().toString())
                }
                adapter?.notifyDataSetChanged()

            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun showDataDialog(){
        var dbref = FirebaseDatabase.getInstance().getReference().child("CF_DayaTarik")
        dbref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                arrayList2Spin.clear()
                for (data in dataSnapshot.children) {
                    list2Spin?.add(data.child("nama_dayatarik").getValue().toString())
                    list22Spin?.add(data.child("kode_dayatarik").getValue().toString())
                }
                val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                        applicationContext,
                        android.R.layout.simple_spinner_item,
                        list2Spin!!
                )
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                editsearchableSpinner.setAdapter(adapter)
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })

        editsearchableSpinner.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
            ) {
                (parent.getChildAt(0) as TextView).setTextColor(Color.WHITE) /* if you want your item to be white */
                //tampung_dayatarik = parent.getItemAtPosition(position).toString()
                tampung_dayatarikSpin = list22Spin?.get(position).toString()
                Toast.makeText(parent.getContext(), tampung_dayatarikSpin, Toast.LENGTH_SHORT).show();
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
    }

    fun Delete_NilaiDayaTarik(){
        btn_deletenilaidayatarikedit.setOnClickListener {
            val mydatabase = FirebaseDatabase.getInstance().getReference("CF_Rule").child(
                tampungkode_rule!!
            ).removeValue()
            Toast.makeText(this,"Terhapus", Toast.LENGTH_SHORT).show()
            val i = Intent(this, Adm_NilaiDayaTarikEdit::class.java)
            startActivity(i)
            finish()
        }
    }

    fun Edit_NilaiDayaTarik(){
        btn_editnilaidayatarik_unggah.setOnClickListener {
            val mDatabase = tampungkode_rule?.let {
                FirebaseDatabase.getInstance().getReference("CF_Rule").child(
                    it
                )
            }
            tampungkode_wisata = edt_editnilaidayatarik_kodewisata.getText().toString().trim()
            tampung_nilai = edt_editnilaiayatarik_nilai.getText().toString().trim()
            mDatabase?.child("id_rule")?.setValue(tampungkode_rule?.toDouble())
            mDatabase?.child("kode_dayatarik")?.setValue(tampung_dayatarikSpin.toString().trim())
            mDatabase?.child("kode_wisata")?.setValue(tampungkode_wisata)
            mDatabase?.child("nilai_cf")?.setValue(tampung_nilai)

        }
    }


}