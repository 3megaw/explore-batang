package com.example.explorebatang

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.explorebatang.Data.Data_Wisata
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_adm__add_destinasi.*
import kotlinx.android.synthetic.main.activity_adm__destinasi_edit.*
import java.util.HashMap


class Adm_DestinasiEdit : AppCompatActivity() {

    lateinit var filePath: Uri
    private var Folder: StorageReference? = null
    private var tampungkey: Int? = null
    private var keyString:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm__destinasi_edit)

        Folder = FirebaseStorage.getInstance().reference.child("Wisata")

        val i = this.intent
        edt_editdestinasi_kodewisata.setText(i.extras?.getString("kode"))
        keyString = i.extras?.getString("kode")
        keyString = keyString!!.replace("W","")
        edt_editdestinasi_namawisata.setText(i.extras?.getString("namawisata"))
        edt_editdestinasi_deskripsiwisata.setText(i.extras?.getString("deskripsi"))
        edt_editdestinasi_fasilitaswisata.setText(i.extras?.getString("fasilitas"))
        //img_editdestinasi_preview
        edt_editdestinasi_jeniswisata.setText(i.extras?.getString("jenis"))
        edt_editdestinasi_ketinggianwisata.setText(i.extras?.getString("ketinggian"))
        edt_editdestinasi_lokasiwisata.setText(i.extras?.getString("lokasi"))
        edt_editdestinasi_latitudewisata.setText(i.extras?.getString("lat"))
        edt_editdestinasi_longitudewisata.setText(i.extras?.getString("long"))
        edt_editdestinasi_wakturamaiwisata.setText(i.extras?.getString("wakturamai"))
        edt_editdestinasi_tiketwisata.setText(i.extras?.getString("tiket"))
        edt_editdestinasi_tiketdetailwisata.setText(i.extras?.getString("tiketdetail"))

        /*databaseReference!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                for (postsnap in snapshot.children) {
                    val dataWisata: Data_Wisata? = postsnap.getValue(Data_Wisata::class.java)
                    if (dataWisata?.kode_wisata.equals(edt_editdestinasi_kodewisata.text.toString())) {
                        edt_editdestinasi_namawisata.setText("SAMA")
                    }else{
                        edt_editdestinasi_namawisata.setText("BEDA "+dataWisata?.kode_wisata)
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) {
                // calling on cancelled method when we receive
                // any error or we are not able to get the data.
                Toast.makeText(this@Adm_DestinasiEdit, "Fail to get data.", Toast.LENGTH_SHORT).show()
            }
        })*/

        btn_editdestinasi_loadimg.setOnClickListener {
            startFileChooser()
        }
        btn_deletedestinasi.setOnClickListener {
            val mydatabase = FirebaseDatabase.getInstance().getReference("CF_Wisata").child(
                keyString!!
            ).removeValue()
            Toast.makeText(this,"Terhapus",Toast.LENGTH_SHORT).show()
            val i = Intent(this, Adm_Destinasi::class.java)
            startActivity(i)
            finish()
        }
        btn_editdestinasi_unggah.setOnClickListener(View.OnClickListener {
            val Imagename: StorageReference = Folder!!.child("image" + filePath.getLastPathSegment())
            Imagename.putFile(filePath).addOnSuccessListener {
                Imagename.downloadUrl.addOnSuccessListener { uri ->
                    val imagestore = keyString?.let { it1 -> FirebaseDatabase.getInstance().getReference("CF_Wisata").child(it1) }
                    val hashMap = HashMap<String, String>()
                    hashMap["image_wisata"] = uri.toString()
                    hashMap["deskripsi"] = edt_editdestinasi_deskripsiwisata.getText().toString().trim { it <= ' ' }
                    hashMap["fasilitas"] = edt_editdestinasi_fasilitaswisata.getText().toString().trim { it <= ' ' }
                    hashMap["jenis_wisata"] = edt_editdestinasi_jeniswisata.getText().toString().trim { it <= ' ' }
                    hashMap["ketinggian"] = edt_editdestinasi_ketinggianwisata.getText().toString().trim { it <= ' ' }
                    hashMap["kode_wisata"] = edt_editdestinasi_kodewisata.getText().toString().trim { it <= ' ' }
                    hashMap["lat_wisata"] = edt_editdestinasi_latitudewisata.getText().toString().trim { it <= ' ' }
                    hashMap["lokasi_wisata"] = edt_editdestinasi_lokasiwisata.getText().toString().trim { it <= ' ' }
                    hashMap["long_wisata"] = edt_editdestinasi_longitudewisata.getText().toString().trim { it <= ' ' }
                    hashMap["nama_wisata"] = edt_editdestinasi_namawisata.getText().toString().trim { it <= ' ' }
                    hashMap["tiket"] = edt_editdestinasi_tiketwisata.getText().toString().trim { it <= ' ' }
                    hashMap["tiketdetail"] = edt_editdestinasi_tiketdetailwisata.getText().toString().trim { it <= ' ' }
                    hashMap["wakturamai"] = edt_editdestinasi_wakturamaiwisata.getText().toString().trim { it <= ' ' }
                    if (imagestore != null) {
                        imagestore.setValue(hashMap).addOnSuccessListener {
                            Toast.makeText(this, "Data berhasil diubah", Toast.LENGTH_SHORT).show()
                            hashMap.clear()
                        }
                    }
                }
            }
        })
    }

    private fun startFileChooser() {
        var i = Intent()
        i.setType("image/*")
        i.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(Intent.createChooser(i, "Pilih foto"), 111)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==111&&resultCode== Activity.RESULT_OK && data != null){
            filePath = data.data!!
            var bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
            img_editdestinasi_preview.setImageBitmap(bitmap)
        }
    }
}