package com.example.explorebatang

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_adm__add_destinasi.*
import kotlinx.android.synthetic.main.activity_wisata__detail.*
import java.util.*


class Adm_DestinasiAdd : AppCompatActivity() {

    lateinit var filePath: Uri
    private var Folder: StorageReference? = null
    private var tampungkey: Int? = null
    private var keyString:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm__add_destinasi)

        Folder = FirebaseStorage.getInstance().reference.child("Wisata")

        btn_adddestinasi_loadimg.setOnClickListener {
            startFileChooser()
        }
        btn_back.setOnClickListener {
            finish()
        }

        getLastKodeWisata()

        btn_adddestinasi_unggah.setOnClickListener(View.OnClickListener {
            val Imagename: StorageReference = Folder!!.child("image" + filePath.getLastPathSegment())
            Imagename.putFile(filePath).addOnSuccessListener {
                Imagename.downloadUrl.addOnSuccessListener { uri ->
                    val imagestore = keyString?.let { it1 -> FirebaseDatabase.getInstance().getReference("CF_Wisata").child(it1) }
                    val hashMap = HashMap<String, String>()
                    hashMap["image_wisata"] = uri.toString()
                    hashMap["deskripsi"] = edt_adddestinasi_deskripsiwisata.getText().toString().trim { it <= ' ' }
                    hashMap["fasilitas"] = edt_adddestinasi_fasilitaswisata.getText().toString().trim { it <= ' ' }
                    hashMap["jenis_wisata"] = edt_adddestinasi_jeniswisata.getText().toString().trim { it <= ' ' }
                    hashMap["ketinggian"] = edt_adddestinasi_ketinggianwisata.getText().toString().trim { it <= ' ' }
                    hashMap["kode_wisata"] = edt_adddestinasi_kodewisata.getText().toString().trim { it <= ' ' }
                    hashMap["lat_wisata"] = edt_adddestinasi_latitudewisata.getText().toString().trim { it <= ' ' }
                    hashMap["lokasi_wisata"] = edt_adddestinasi_lokasiwisata.getText().toString().trim { it <= ' ' }
                    hashMap["long_wisata"] = edt_adddestinasi_longitudewisata.getText().toString().trim { it <= ' ' }
                    hashMap["nama_wisata"] = edt_adddestinasi_namawisata.getText().toString().trim { it <= ' ' }
                    hashMap["tiket"] = edt_adddestinasi_tiketwisata.getText().toString().trim { it <= ' ' }
                    hashMap["tiketdetail"] = edt_adddestinasi_tiketdetailwisata.getText().toString().trim { it <= ' ' }
                    hashMap["wakturamai"] = edt_adddestinasi_wakturamaiwisata.getText().toString().trim { it <= ' ' }
                    if (imagestore != null) {
                        imagestore.setValue(hashMap).addOnSuccessListener {
                            Toast.makeText(this, "Terunggah", Toast.LENGTH_SHORT).show()
                            hashMap.clear()
                        }
                    }
                    getLastKodeWisata()
                    clearTV()
                }
            }
        })
    }

    private fun getLastKodeWisata(){
        var dbref = FirebaseDatabase.getInstance().getReference().child("CF_Wisata")
        var lastquery: Query = dbref.orderByKey().limitToLast(1)
        lastquery.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (data in dataSnapshot.children) {
                    tampungkey = data.key?.toInt()
                }
                keyString = (tampungkey?.plus(1)).toString()
                edt_adddestinasi_kodewisata.setText("W"+keyString)
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun clearTV(){
        edt_adddestinasi_namawisata.setText("")
        edt_adddestinasi_deskripsiwisata.setText("")
        edt_adddestinasi_fasilitaswisata.setText("")
        edt_adddestinasi_jeniswisata.setText("")
        edt_adddestinasi_ketinggianwisata.setText("")
        edt_adddestinasi_latitudewisata.setText("")
        edt_adddestinasi_lokasiwisata.setText("")
        edt_adddestinasi_longitudewisata.setText("")
        edt_adddestinasi_tiketdetailwisata.setText("")
        edt_adddestinasi_tiketwisata.setText("")
        edt_adddestinasi_wakturamaiwisata.setText("")
    }

    private fun startFileChooser() {
        var i = Intent()
        i.setType("image/*")
        i.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(Intent.createChooser(i, "Pilih foto"), 111)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==111&&resultCode==Activity.RESULT_OK && data != null){
            filePath = data.data!!
            var bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
            img_adddestinasi_preview.setImageBitmap(bitmap)
        }
    }

   /* private fun uploadFile(){
        if(filePath!=null){
            var pd = ProgressDialog(this)
            pd.setTitle("Menggunggah")
            pd.show()
            var imageRef = FirebaseStorage.getInstance().reference.child("")

            imageRef.putFile(filePath)
                    .addOnSuccessListener { p0->
                        pd.dismiss()
                        Toast.makeText(applicationContext, "Foto terunggah", Toast.LENGTH_LONG).show()
                    }
                    .addOnFailureListener{ p0->
                        pd.dismiss()
                        Toast.makeText(applicationContext, p0.message, Toast.LENGTH_SHORT).show()
                    }
                    .addOnProgressListener { p0->
                        var progress = (100.0 * p0.bytesTransferred)/p0.totalByteCount
                        pd.setMessage("Terunggah ${progress.toInt()}%")
                    }
        }
    }*/
}