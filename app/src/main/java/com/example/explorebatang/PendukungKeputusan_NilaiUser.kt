package com.example.explorebatang

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.explorebatang.adapter.BasicSpinnerAdapter
import kotlinx.android.synthetic.main.activity_pendukung_keputusan__nilai_user.*


@Suppress("ClassName")
class PendukungKeputusan_NilaiUser : AppCompatActivity() {
    var semuaDayaTarikTerpilih = arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pendukung_keputusan__nilai_user)

        getDayaTarikTerpilih()
        initButton()
        btn_back_keputusannilaiuser.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initButton() {
        btn_hasilspk.setOnClickListener {
            NavigationController.navigateToPendukungKeputusanHasil(
                this,
                semuaDayaTarikTerpilih,
                (list_dayatarik_cfuser.adapter as BasicSpinnerAdapter).listSelectedPossibilites
            )

        }
    }

    private fun getDayaTarikTerpilih() {
        val extraDayaTarik = intent.getStringExtra(EXTRA_HASIL)
        if (extraDayaTarik != null) {
            val semuaDayaTarik = extraDayaTarik.split("#")
            for (dayatarik in semuaDayaTarik) {
                if (dayatarik.isNotEmpty()) {
                    semuaDayaTarikTerpilih.add(dayatarik)
                }
            }
            val adapter = BasicSpinnerAdapter(semuaDayaTarikTerpilih, this)
            list_dayatarik_cfuser.adapter = adapter
        }
    }

    companion object {
        const val EXTRA_HASIL = "HASIL"
    }
}