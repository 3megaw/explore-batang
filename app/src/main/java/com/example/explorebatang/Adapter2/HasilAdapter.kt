package com.example.explorebatang.Adapter2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.R

class HasilAdapter(context: Context, var data: List<ItemHasil>): RecyclerView.Adapter<HasilAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    var clickListener: ItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = mInflater.inflate(R.layout.item_hasil, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val datum = data[position]
        holder.tvTitle.text = datum.name
        holder.tvPercentage.text = datum.percentage
    }

    override fun getItemCount():Int{
        return data.size
    }

    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        var tvTitle: TextView = itemView.findViewById(R.id.tv_hasil_name)
        var tvPercentage: TextView = itemView.findViewById(R.id.tv_percentage)
        override fun onClick(view: View?) {
            if (clickListener != null) clickListener!!.onItemClick(view, data[adapterPosition].name)
        }

        init {
            itemView.setOnClickListener(this)
        }
    }

    data class ItemHasil(
        val name:String, val percentage:String
    )

    interface ItemClickListener {
        fun onItemClick(view: View?, wisataName: String)
    }
}