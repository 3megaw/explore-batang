package com.example.explorebatang

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_adm__dayatarikk_edit.*
import kotlinx.android.synthetic.main.activity_adm__destinasi_edit.*
import java.util.HashMap

class Adm_DayatarikEdit : AppCompatActivity() {

    private var keyString:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm__dayatarikk_edit)

        val i = this.intent
        edt_editdayatarik_kodedayatarik.setText(i.extras?.getString("kode_dayatarik"))
        edt_editdayatarik_namadayatarik.setText(i.extras?.getString("nama_dayatarik"))
        keyString = i.extras?.getString("kode_dayatarik")
        keyString = keyString!!.replace("T","")

        btn_deletedayatarik.setOnClickListener {
            val mydatabase = FirebaseDatabase.getInstance().getReference("CF_DayaTarik").child(
                    keyString!!
            ).removeValue()
            Toast.makeText(this,"Terhapus", Toast.LENGTH_SHORT).show()
            val i = Intent(this, Adm_Dayatarik::class.java)
            startActivity(i)
            finish()
        }

        btnback_dayatarikedit.setOnClickListener {
            finish()
        }

        btn_editdayatarik_unggah.setOnClickListener {
            val datastore = keyString?.let { it1 -> FirebaseDatabase.getInstance().getReference("CF_DayaTarik").child(it1) }
            val hashMap = HashMap<String, String>()
            hashMap["kode_dayatarik"] = edt_editdayatarik_kodedayatarik.getText().toString().trim { it <= ' ' }
            hashMap["nama_dayatarik"] = edt_editdayatarik_namadayatarik.getText().toString().trim { it <= ' ' }

            if (datastore != null) {
                datastore.setValue(hashMap).addOnSuccessListener {
                    Toast.makeText(this, "Data berhasil diubah", Toast.LENGTH_SHORT).show()
                    hashMap.clear()
                }
            }
        }
    }
}