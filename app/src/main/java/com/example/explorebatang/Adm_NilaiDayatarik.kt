package com.example.explorebatang

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.explorebatang.Data.Data_Wisata
import com.example.explorebatang.Holder.Holder_AdmWisata
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_adm__nilai_dayatarik.*

class Adm_NilaiDayatarik : AppCompatActivity() {

    lateinit var adapter2: FirebaseRecyclerAdapter<Data_Wisata, Holder_AdmWisata>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm__nilai_dayatarik)

        btn_addadmnilaidayatarik.setOnClickListener {
            var intent = Intent(this, Adm_NilaiDayatarikAdd::class.java)
            startActivity(intent)
        }
        btnback_pengetahuan.setOnClickListener {
            finish()
        }

        Load_admNilaiDayaTarik()
    }

    private fun Load_admNilaiDayaTarik(){
        val options = FirebaseRecyclerOptions.Builder<Data_Wisata>()
            .setQuery(
                FirebaseDatabase.getInstance().getReference().child("CF_Wisata"),
                Data_Wisata::class.java
            )
            .build()
        adapter2 = object : FirebaseRecyclerAdapter<Data_Wisata, Holder_AdmWisata>(options) {
            override fun onBindViewHolder(
                holder: Holder_AdmWisata,
                position: Int,
                model: Data_Wisata
            ) {
                holder.nama_wisata.setText(model.nama_wisata)
                holder.lokasi_wisata.setText(model.lokasi_wisata)
                holder.listwisata.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putString("kodewisata", model.kode_wisata)

                    val intent = Intent(this@Adm_NilaiDayatarik, Adm_NilaiDayaTarikEdit::class.java)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder_AdmWisata {
                val v = LayoutInflater.from(parent.context).inflate(
                    R.layout.card_adm_destinasi,
                    parent,
                    false
                )
                return Holder_AdmWisata(v)
            }
        }
        adapter2.startListening()
        rec_admnilaidayatarik.setAdapter(adapter2)
    }
}