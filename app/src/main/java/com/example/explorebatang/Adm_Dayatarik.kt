package com.example.explorebatang

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.explorebatang.Data.Data_DayaTarik
import com.example.explorebatang.Holder.Holder_AdmDayaTarik
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_adm__dayatarik.*
import kotlinx.android.synthetic.main.activity_adm__destinasi.*

class Adm_Dayatarik : AppCompatActivity() {

    lateinit var adapter2: FirebaseRecyclerAdapter<Data_DayaTarik, Holder_AdmDayaTarik>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm__dayatarik)

        btn_addadmdayatarik.setOnClickListener {
            var intent = Intent(this, Adm_DayatarikAdd::class.java)
            startActivity(intent)
        }
        imageView7.setOnClickListener {
            finish()
        }

        Load_admDayaTarik()
    }

    private fun Load_admDayaTarik(){
        val options = FirebaseRecyclerOptions.Builder<Data_DayaTarik>()
            .setQuery(
                FirebaseDatabase.getInstance().getReference().child("CF_DayaTarik"),
                Data_DayaTarik::class.java
            )
            .build()
        adapter2 = object : FirebaseRecyclerAdapter<Data_DayaTarik, Holder_AdmDayaTarik>(options) {
            override fun onBindViewHolder(
                holder: Holder_AdmDayaTarik,
                position: Int,
                model: Data_DayaTarik
            ) {
                holder.kode_dayatarik.setText(model.kode_dayatarik)
                holder.nama_dayatarik.setText(model.nama_dayatarik)
                holder.list_dayatarik.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putString("kode_dayatarik", model.kode_dayatarik)
                    bundle.putString("nama_dayatarik", model.nama_dayatarik)
                    val intent = Intent(this@Adm_Dayatarik, Adm_DayatarikEdit::class.java)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder_AdmDayaTarik {
                val v = LayoutInflater.from(parent.context).inflate(
                    R.layout.card_adm_dayatarik,
                    parent,
                    false
                )
                return Holder_AdmDayaTarik(v)
            }
        }
        adapter2.startListening()
        rec_admdayatarik.setAdapter(adapter2)
    }
}