package com.example.explorebatang

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.explorebatang.Data.DataModel_Komentar
import com.example.explorebatang.Holder.Holder_AdmKomentar
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_adm__destinasi.*
import kotlinx.android.synthetic.main.activity_adm__komentar.*

class Adm_Komentar : AppCompatActivity() {

    lateinit var adapter2: FirebaseRecyclerAdapter<DataModel_Komentar, Holder_AdmKomentar>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm__komentar)

        load_AdmKomentar()
        btn_backkomentar.setOnClickListener {
            finish()
        }
    }

    private fun load_AdmKomentar(){
        val options = FirebaseRecyclerOptions.Builder<DataModel_Komentar>()
            .setQuery(
                FirebaseDatabase.getInstance().getReference().child("Komentar"),
                DataModel_Komentar::class.java
            )
            .build()
        adapter2 = object : FirebaseRecyclerAdapter<DataModel_Komentar, Holder_AdmKomentar>(options) {
            override fun onBindViewHolder(
                holder: Holder_AdmKomentar,
                position: Int,
                model: DataModel_Komentar
            ) {
                holder.komentar.setText(model.komentar)
                holder.tanggalkomentar.setText(model.tanggal)
                holder.wisatakomentar.setText(model.wisata)
                holder.listkomentar.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putString("wisata",model.wisata)
                    bundle.putString("komentar",model.komentar)
                    bundle.putString("tanggal",model.tanggal)

                    val intent = Intent(this@Adm_Komentar, Adm_DestinasiEdit::class.java)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder_AdmKomentar {
                val v = LayoutInflater.from(parent.context).inflate(
                    R.layout.card_adm_komentar,
                    parent,
                    false
                )
                return Holder_AdmKomentar(v)
            }
        }
        adapter2.startListening()
        rec_admkomentar.setAdapter(adapter2)
    }
}