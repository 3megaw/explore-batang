package com.example.explorebatang

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.example.explorebatang.Data.Data_Wisata
import com.example.explorebatang.Holder.Holder_AdmWisata
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_adm__add_destinasi.*
import kotlinx.android.synthetic.main.activity_adm__destinasi.*


class Adm_Destinasi : AppCompatActivity() {

    lateinit var adapter2: FirebaseRecyclerAdapter<Data_Wisata, Holder_AdmWisata>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm__destinasi)

        btn_addadmdestinasi.setOnClickListener {
            val intent=Intent(this, Adm_DestinasiAdd::class.java)
            startActivity(intent)
        }
        btn_backdestinasi.setOnClickListener {
            finish()
        }

        Load_admDestinasi()
    }

    private fun Load_admDestinasi(){
        val options = FirebaseRecyclerOptions.Builder<Data_Wisata>()
            .setQuery(
                FirebaseDatabase.getInstance().getReference().child("CF_Wisata"),
                Data_Wisata::class.java
            )
            .build()
        adapter2 = object : FirebaseRecyclerAdapter<Data_Wisata, Holder_AdmWisata>(options) {
            override fun onBindViewHolder(
                holder: Holder_AdmWisata,
                position: Int,
                model: Data_Wisata
            ) {
                holder.nama_wisata.setText(model.nama_wisata)
                holder.lokasi_wisata.setText(model.lokasi_wisata)
                holder.listwisata.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putString("namawisata", model.nama_wisata)
                    bundle.putString("deskripsi", model.deskripsi)
                    bundle.putString("fasilitas", model.fasilitas)
                    bundle.putString("gambar", model.image_wisata)
                    bundle.putString("jenis", model.jenis_wisata)
                    bundle.putString("ketinggian", model.ketinggian)
                    bundle.putString("kode",model.kode_wisata)
                    bundle.putString("lokasi",model.lokasi_wisata)
                    bundle.putString("lat", model.lat_wisata)
                    bundle.putString("long", model.long_wisata)
                    bundle.putString("wakturamai", model.wakturamai)
                    bundle.putString("tiket",model.tiket)
                    bundle.putString("tiketdetail",model.tiketdetail)

                    val intent = Intent(this@Adm_Destinasi, Adm_DestinasiEdit::class.java)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder_AdmWisata {
                val v = LayoutInflater.from(parent.context).inflate(
                    R.layout.card_adm_destinasi,
                    parent,
                    false
                )
                return Holder_AdmWisata(v)
            }
        }
        adapter2.startListening()
        rec_admdestinasi.setAdapter(adapter2)
    }
}