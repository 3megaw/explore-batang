package com.example.explorebatang

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.explorebatang.Adapter2.HasilAdapter
import com.example.explorebatang.NavigationController.EXTRA_LIST_SELECTED_DAYA_TARIK
import com.example.explorebatang.NavigationController.EXTRA_LIST_SELECTED_POSSIBILITY
import com.example.explorebatang.Data.WisataModel
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_daftar.*
import kotlinx.android.synthetic.main.activity_pendukung_keputusan__hasil.*
import java.util.*
import kotlin.collections.HashMap

class PendukungKeputusan_Hasil : AppCompatActivity() {

    // Listener untuk callback setiap read firebase data
    lateinit var listener: Listener

    private var dbWisataReference: DatabaseReference? = null
    private var dbRuleReference: DatabaseReference? = null
    private var dbDayaTarikReference: DatabaseReference? = null
    private var semuaDayaTarikTerpilih = arrayListOf<String>()
    private var semuaKodeDayaTarikTerpilih = arrayListOf<String>()
    private var semuaNilaiPossibilityDayaTarikTerpilih = arrayListOf<String>()
    private val listSemuaWisata = arrayListOf<WisataModel>()
    var cfGabungan = HashMap<String, Double>()
    private lateinit var db: FirebaseDatabase

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pendukung_keputusan__hasil)

        db = FirebaseDatabase.getInstance()
        dbWisataReference = db.reference.child("CF_Wisata")
        dbRuleReference = db.reference.child("CF_Rule")
        dbDayaTarikReference = db.reference.child("CF_DayaTarik")

        //get nilai daya tarik terpilih
        semuaNilaiPossibilityDayaTarikTerpilih = intent.getStringArrayListExtra(EXTRA_LIST_SELECTED_POSSIBILITY)
        //get daya tarik terpilih
        semuaDayaTarikTerpilih = intent.getStringArrayListExtra(EXTRA_LIST_SELECTED_DAYA_TARIK)

        //show daya tarik terpilih
        val arraySize: Int = semuaDayaTarikTerpilih.size
        for (i in 0 until arraySize) {
            tv_list_dayatarik_dipilih.append("\u2022 ${semuaDayaTarikTerpilih.get(i)}\n")
        }

        // Init Listener wrap
        listenerFlow()

        // get data semua wisata
        getSemuaWisata()
    }

    private fun getSemuaWisata() {
        val semuaWisata = hashMapOf<String, String>()
        val queryCariwisata = dbWisataReference!!.orderByChild("kode_wisata")
        queryCariwisata.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot4: DataSnapshot) {
                for (postSnapshot4 in snapshot4.children) {
                    val namaWisata = postSnapshot4.child("nama_wisata").value as String?
                    val kodeWisata = postSnapshot4.child("kode_wisata").value as String?

                    // tambah wisata ke hashmap
                    semuaWisata[namaWisata!!] = kodeWisata!!

                    // tambah ke list utk next activity
                    val wisataModel = WisataModel().also { it.parseToModel(postSnapshot4) }
                    listSemuaWisata.add(wisataModel)
                }
                // panggil callback setelah semua diquery
                listener.allWisataRetrieved(semuaWisata)
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }

    private fun listenerFlow() {
        listener = object : Listener {
            override fun allWisataRetrieved(semuaWisata: HashMap<String, String>) {
                getAllRule(semuaWisata)
            }

            @SuppressLint("SetTextI18n")
            override fun allRuleIterated(
                    semuaWisata: java.util.HashMap<String, String>,
                    mapRuleDayaTarik: HashMap<Int, String>,
                    mapRuleCF: HashMap<Int, Double>,
                    mapRuleWisata: HashMap<Int, String>
            ) {
                getKodeDayaTarikTerpilih(semuaWisata, mapRuleDayaTarik, mapRuleCF, mapRuleWisata)
            }

            override fun allKodeDayaTarikRetrieved(
                    semuaWisata: java.util.HashMap<String, String>,
                    mapRuleDayaTarik: HashMap<Int, String>,
                    mapRuleCF: HashMap<Int, Double>,
                    mapRuleWisata: HashMap<Int, String>
            ) {
                calculateCFandAddToHashMap(semuaWisata, mapRuleDayaTarik, mapRuleCF, mapRuleWisata)
            }

            @SuppressLint("SetTextI18n")
            override fun showHighestCFtoUI(mapHasil: HashMap<String?, Double>) {
                val sortedWisata = mapHasil.sortDescByValue()
                val listHasil = arrayListOf<HasilAdapter.ItemHasil>()
                sortedWisata?.forEach {
                    listHasil.add(HasilAdapter.ItemHasil(it.key, it.value.toInt().toString() + "%"))
                }
                //show to recycler
                rv_hasil.apply {
                    layoutManager = LinearLayoutManager(this@PendukungKeputusan_Hasil)
                    adapter = HasilAdapter(this@PendukungKeputusan_Hasil, listHasil).also { hasilAdapter ->
                        hasilAdapter.clickListener = object : HasilAdapter.ItemClickListener{
                            override fun onItemClick(view: View?, wisataName: String) {
                                val wisataModel = listSemuaWisata.find { it.nama_wisata == wisataName }
                                if (wisataModel != null) {
                                    NavigationController.navigateToWisataDetail(this@PendukungKeputusan_Hasil, wisataModel)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun getAllRule(semuaWisata: java.util.HashMap<String, String>) {
        val mapRuleDayaTarik = HashMap<Int, String>()
        val mapRuleCF = HashMap<Int, Double>()
        val mapRuleWisata = HashMap<Int, String>()
        var iteration = 1
        for (wisata in semuaWisata) {
            cfGabungan[wisata.value] = 0.0
            val queryRule = dbRuleReference!!.orderByChild("kode_wisata").equalTo(wisata.value)
            queryRule.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot2: DataSnapshot) {
                    for (postSnaphot2 in snapshot2.children) {
                        val idRule = postSnaphot2.child("id_rule").value.toString().toInt()
                        val kodeDayaTarik = postSnaphot2.child("kode_dayatarik").value as String?
                        val kodeWisata = postSnaphot2.child("kode_wisata").value as String?
                        val cf = postSnaphot2.child("nilai_cf").value.toString().toDouble()
                        mapRuleDayaTarik[idRule] = kodeDayaTarik!!
                        mapRuleWisata[idRule] = kodeWisata!!
                        mapRuleCF[idRule] = cf
                    }

                    if (iteration == semuaWisata.size) {
                        listener.allRuleIterated(
                                semuaWisata,
                                mapRuleDayaTarik,
                                mapRuleCF,
                                mapRuleWisata
                        )
                    } else {
                        iteration++
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.w("ERROR", "loadPost:onCancelled", error.toException())
                }
            })
        }
    }

    private fun getKodeDayaTarikTerpilih(
            semuaWisata: java.util.HashMap<String, String>,
            mapRuleDayaTarik: HashMap<Int, String>,
            mapRuleCF: HashMap<Int, Double>,
            mapRuleWisata: HashMap<Int, String>
    ) {
        var iteration = 1
        for (dayatarikTerpilih in semuaDayaTarikTerpilih) {
            val queryDayatarik =
                    dbDayaTarikReference!!.orderByChild("nama_dayatarik").equalTo(dayatarikTerpilih)
            queryDayatarik.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot1: DataSnapshot) {
                    for (postSnapshot1 in snapshot1.children) {
                        val kodeDayatarik = postSnapshot1.child("kode_dayatarik").value as String?
                        semuaKodeDayaTarikTerpilih.add(kodeDayatarik!!)
                    }
                    if (iteration == semuaDayaTarikTerpilih.size) {
                        // Panggil Callback Setelah semua Kode daya tarik Di Query
                        listener.allKodeDayaTarikRetrieved(
                                semuaWisata,
                                mapRuleDayaTarik,
                                mapRuleCF,
                                mapRuleWisata
                        )
                    } else {
                        iteration++
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.w("ERROR", "loadPost:onCancelled", error.toException())
                }
            })
        }
    }

    private fun calculateCFandAddToHashMap(
            semuaWisata: java.util.HashMap<String, String>,
            mapRuleDayaTarik: HashMap<Int, String>,
            mapRuleCF: HashMap<Int, Double>,
            mapRuleWisata: HashMap<Int, String>
    ) {
        val mapHasil = HashMap<String?, Double>() //untuk menyimpan hasil penilaian CF
        for (idRule in mapRuleCF.keys) {
            for ((dayatarikke, kodeDayatarik) in semuaKodeDayaTarikTerpilih.withIndex()) {
                //cek kesamaan pada rule dan daya tarik terpilih
                if (mapRuleDayaTarik[idRule].equals(kodeDayatarik)) {
                    val cf = mapRuleCF[idRule]!! * semuaNilaiPossibilityDayaTarikTerpilih[dayatarikke].toPossibilityValue() // UPDATED
                    val kodeWisata = mapRuleWisata[idRule]!!
                    cfGabungan[kodeWisata] = when {
                        dayatarikke > 1 -> cf + (cfGabungan[kodeWisata]!! * (1 - cf))
                        dayatarikke == 1 -> cfGabungan[kodeWisata]!! + cf * (1 - cfGabungan[kodeWisata]!!)
                        else -> cf
                    }
                    val namaWisata = semuaWisata.getKeyFromValue(mapRuleWisata[idRule])
                    mapHasil[namaWisata] = cfGabungan[mapRuleWisata[idRule]!!]!! * 100
                }
            }
        }
        listener.showHighestCFtoUI(mapHasil)
    }

    private fun HashMap<String?, Double>.sortDescByValue(): java.util.HashMap<String, Double>? {
        val list: LinkedList<Map.Entry<String?, Double>> =
                LinkedList<Map.Entry<String?, Double>>(this.entries)

        list.sortWith { o1, o2 ->
            o2.value.compareTo(o1.value)
        }

        val temp: java.util.HashMap<String, Double> = LinkedHashMap()
        for ((key, value) in list) {
            temp[key!!] = value
        }
        return temp
    }

    private fun <K, V> HashMap<K, V>.getKeyFromValue(kodeWisata: String?): K? {
        for (map in this.entries) {
            if (map.value == kodeWisata) {
                return map.key
            }
        }
        return null
    }

    private fun String.toPossibilityValue(): Double {
        return when (this) {
            "Mungkin" -> return 0.6
            "Hampir Pasti" -> return 0.8
            "Pasti" -> return 1.0
            else -> 0.0
        }
    }

    interface Listener {
        fun allWisataRetrieved(semuaWisata: HashMap<String, String>)
        fun allRuleIterated(
                semuaWisata: java.util.HashMap<String, String>,
                mapRuleDayaTarik: HashMap<Int, String>,
                mapRuleCF: HashMap<Int, Double>,
                mapRuleWisata: HashMap<Int, String>
        )

        fun showHighestCFtoUI(mapHasil: HashMap<String?, Double>)
        fun allKodeDayaTarikRetrieved(
                semuaWisata: java.util.HashMap<String, String>,
                mapRuleDayaTarik: HashMap<Int, String>,
                mapRuleCF: HashMap<Int, Double>,
                mapRuleWisata: HashMap<Int, String>
        )
    }
}

