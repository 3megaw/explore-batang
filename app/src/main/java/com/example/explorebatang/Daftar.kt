package com.example.explorebatang

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_daftar.*

class Daftar : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar)

        auth = FirebaseAuth.getInstance()

        btn_daftar.setOnClickListener {
            createUser()
        }
        txt_masuk.setOnClickListener {
            val i = Intent(this,Login::class.java)
            startActivity(i)
        }
    }

    private fun createUser(){
        val email = edt_email.text.toString()
        val pass = edt_pass.text.toString()

        if(email.isNotEmpty() && pass.isNotEmpty()){
            auth.createUserWithEmailAndPassword(email,pass)
                .addOnCompleteListener({ taskId->
                    if(taskId.isSuccessful){
                        //user registered to firebase
                        val firebaseUser: FirebaseUser = taskId.result!!.user!!
                        Toast.makeText(this,"Registrasi berhasil",Toast.LENGTH_SHORT).show()
                    }
                })
        }
    }

}