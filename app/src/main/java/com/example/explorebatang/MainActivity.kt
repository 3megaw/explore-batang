package com.example.explorebatang

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.explorebatang.Data.Data_JenisWisata
import com.example.explorebatang.Holder.Holder_JenisWisata
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.rec_jeniswisata.*


class MainActivity : AppCompatActivity() {

    //    lateinit var adapterJeniswisata: Adapter_JenisWisata
    lateinit var recyclerView : RecyclerView
    lateinit var adapter2: FirebaseRecyclerAdapter<Data_JenisWisata, Holder_JenisWisata>

    // var lokasi
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var lastLocation: Location? = null
    private var latitudeText: TextView? = null
    private var longitudeText: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        latitudeText = findViewById<View>(R.id.txt_latitude) as TextView
        longitudeText = findViewById<View>(R.id.txt_longitude) as TextView

        mulaiSPK.setOnClickListener {
            val intent = Intent(this, PendukungKeputusan_DayaTarik::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
        }

        btn_menu.setOnClickListener {
            val intent = Intent(this, SideMenu::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }

        recyclerView = findViewById(R.id.rec_jeniswisata)
        recyclerView.layoutManager = LinearLayoutManager(
                this,
                LinearLayoutManager.HORIZONTAL,
                false
        )
        recyclerView.setHasFixedSize(true)
        recyclerView.overScrollMode = View.OVER_SCROLL_NEVER

        LoadData_JenisWisata()
    }

    private fun LoadData_JenisWisata(){
        val options = FirebaseRecyclerOptions.Builder<Data_JenisWisata>()
                .setQuery(
                        FirebaseDatabase.getInstance().reference.child("JenisWisata"),
                        Data_JenisWisata::class.java
                )
                .build()
        adapter2 = object : FirebaseRecyclerAdapter<Data_JenisWisata, Holder_JenisWisata>(options) {
            override fun onBindViewHolder(
                    holder: Holder_JenisWisata,
                    position: Int,
                    model: Data_JenisWisata
            ) {
                holder.namajeniswisata.setText(model.namajeniswisata)
                Picasso.get().load(model.fotojeniswisata).into(holder.imagejeniswisata)
                holder.cardjeniswisata.setOnClickListener(View.OnClickListener {
                    val bundle = Bundle()
                    bundle.putString("jenis", model.pass)
                    bundle.putString("latgps",txt_latitude.text.toString())
                    bundle.putString("longgps",txt_longitude.text.toString())
                    val intent = Intent(
                        this@MainActivity,
                        Wisata::class.java
                    ) //THIS BISA PAKAI CONTEXT KALAU DI BEDA CLASS
                    intent.putExtras(bundle)
                    startActivity(intent)
                })
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder_JenisWisata {
                val v = LayoutInflater.from(parent.context).inflate(
                        R.layout.rec_jeniswisata,
                        parent,
                        false
                )
                return Holder_JenisWisata(v)
            }
        }
        adapter2.startListening()
        rec_jeniswisata.setAdapter(adapter2)
    }

    public override fun onStart() {
        super.onStart()
        if (!checkPermissions()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions()
            }
        }
        else {
            getLastLocation()
        }
    }
    private fun getLastLocation() {
        if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient?.lastLocation!!.addOnCompleteListener(this) { task ->
            if (task.isSuccessful && task.result != null) {
                lastLocation = task.result
                latitudeText?.setText("" + lastLocation?.latitude)
                longitudeText?.setText("" + lastLocation?.longitude)
//                latitudeText!!.text =  ": " + (lastLocation)!!.latitude
//                longitudeText!!.text = ": " + (lastLocation)!!.longitude
                gpsLatitude = lastLocation?.latitude
                gpsLongitude = lastLocation?.longitude
            }
            else {
                Log.w(TAG, "getLastLocation:exception", task.exception)
                showMessage("Lokasi tidak terdeteksi, mohon aktifkan layanan lokasi Anda")
            }
        }
    }
    private fun showMessage(string: String) {
        val container = findViewById<View>(R.id.linearLayout)
        if (container != null) {
            Toast.makeText(this@MainActivity, string, Toast.LENGTH_LONG).show()
        }
    }
    private fun showSnackbar(
            mainTextStringId: String, actionStringId: String,
            listener: View.OnClickListener
    ) {
        Toast.makeText(this@MainActivity, mainTextStringId, Toast.LENGTH_LONG).show()
    }
    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }
    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }
    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
        )
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")
            showSnackbar("Izinkan akses lokasi untuk mendapatkan fitur jarak ke destinasi", "Okay",
                    View.OnClickListener {
                        startLocationPermissionRequest()
                    })
        }
        else {
            Log.i(TAG, "Requesting permission")
            startLocationPermissionRequest()
        }
    }
    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<String>,
            grantResults: IntArray
    ) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            when {
                grantResults.isEmpty() -> {
                    // If user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.i(TAG, "User interaction was cancelled.")
                }
                grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                    // Permission granted.
                    getLastLocation()
                }
                else -> {
                    showSnackbar("Permission was denied", "Settings",
                            View.OnClickListener {
                                // Build intent that displays the App settings screen.
                                val intent = Intent()
                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                val uri = Uri.fromParts(
                                        "package",
                                        Build.DISPLAY, null
                                )
                                intent.data = uri
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                startActivity(intent)
                            }
                    )
                }
            }
        }
    }
    companion object {
        private val TAG = "LocationProvider"
        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34

        var gpsLatitude: Double? = null
        var gpsLongitude: Double? = null
    }



}