package com.example.explorebatang

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_daftar.*
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()

        btn_masuk.setOnClickListener {
            userLogin()
        }
        txt_daftar.setOnClickListener {
            val intent= Intent(this, Daftar::class.java)
            startActivity(intent)
            finish()
        }
    }
    private fun userLogin(){
        val email = edt_emaillog.text.toString()
        val pass = edt_passlog.text.toString()

        if(email=="admin"&&pass=="admincf"){
            val i = Intent(this,Adm_Menu::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()
        }

        auth.signInWithEmailAndPassword(email,pass).addOnCompleteListener { taskId ->
            if(taskId.isSuccessful){
                Toast.makeText(this,"Selamat datang",Toast.LENGTH_SHORT).show()
                val i = Intent(this, MainActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                /*Bundle
                i.putExtra("userid",auth.currentUser!!.uid)*/
                startActivity(i)
                finish()
            }else{
                Toast.makeText(this,taskId.exception!!.message.toString(),Toast.LENGTH_SHORT).show()
            }

        }
    }
}