package com.example.explorebatang

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_adm__menu.*

class Adm_Menu : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm__menu)

        cs_btndestinasi.setOnClickListener {
            var i = Intent(this, Adm_Destinasi::class.java)
            startActivity(i)
        }
        cs_btndayatarik.setOnClickListener {
            var i = Intent(this, Adm_Dayatarik::class.java)
            startActivity(i)
        }
        cs_btnnilaidayatarik.setOnClickListener {
            var i = Intent(this, Adm_NilaiDayatarik::class.java)
            startActivity(i)
        }
        cs_btnkomentar.setOnClickListener {
            var i = Intent(this,Adm_Komentar::class.java)
            startActivity(i)
        }
        btn_exit.setOnClickListener {
            finish()
        }
    }

}