package com.example.explorebatang

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.explorebatang.Data.Sqlite_DatabaseHandler
import com.example.explorebatang.Data.Sqlite_Model
//import com.example.explorebatang.Data.Sqlite_DatabaseHandler
//import com.example.explorebatang.Data.Sqlite_Model
import com.example.explorebatang.NavigationController.EXTRA_WISATA_MODEL
import com.example.explorebatang.Data.WisataModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_wisata__detail.*
import java.io.ByteArrayOutputStream
import java.util.*
import kotlin.math.*

@Suppress("PrivatePropertyName", "ClassName", "LocalVariableName", "SpellCheckingInspection",
    "unused"
)
class Wisata_Detail : AppCompatActivity(), OnMapReadyCallback {

    private var t_latwisata = ""
    private var t_longwisata = ""
    private var t2_latwisata :Double = 0.0
    private var t2_longiwsata :Double = 0.0
    var twakturamai:String? = null
    private lateinit var img_blink: ImageView
    var gpslat:Double? = null
    var gpslong:Double? = null

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wisata__detail)

        img_blink = findViewById(R.id.bulatmerah)
        img_blink.blink()

        val x = this.intent
        val tampungnama =x.extras?.getString("namawisata")
        if(tampungnama!=null){
            PassingData_WisataDetail()
        }

        val wisataModel = intent.extras?.getParcelable<WisataModel>(EXTRA_WISATA_MODEL)
        if (wisataModel != null) {
            txt_namawisata_detailwisata.text = wisataModel.nama_wisata
            txt_deskripsidetailwisata.text = wisataModel.deskripsi
            txt_fasilitasdetailwisata.text = wisataModel.fasilitas
            Picasso.get().load(wisataModel.image_wisata).into(img_detailwisata)
            txt_ketinggian.text = wisataModel.ketinggian + " mdpl"
            t_latwisata = wisataModel.lat_wisata
            t_longwisata = wisataModel.long_wisata
            gpslat = MainActivity.gpsLatitude
            gpslong = MainActivity.gpsLongitude
            if(gpslat == null || gpslong == null){
                jarak.text = "N/A"
            }else{
                val t_gpslat:Double = gpslat!!.toDouble()
                val t_gpslong:Double = gpslong!!.toDouble()
                t2_latwisata = t_latwisata.toDouble()
                t2_longiwsata = t_longwisata.toDouble()
                distance(t_gpslat, t_gpslong, t2_latwisata, t2_longiwsata)
            }
            cek_gps.text = "$gpslat - $gpslong"

                val wakturamai:String = wisataModel.wakturamai
                if(wakturamai=="-"||wakturamai==""){
                    txt_kondisiwisata.text = "Biasanya tidak ramai"
                    img_blink.setBackgroundResource(R.drawable.circlegreen)
                }else{
                    val splitWaktu: Array<String> = wakturamai.split("-").toTypedArray()
                    val split1 = Integer.parseInt(splitWaktu[0].trim())
                    val split2 = Integer.parseInt(splitWaktu[1].trim())
                    val firstHour: Calendar = Calendar.getInstance()
                    val hour = firstHour.get(Calendar.HOUR_OF_DAY)
                    if(hour in split1..split2){
                        txt_kondisiwisata.text = "Biasanya ramai"
                        img_blink.setBackgroundResource(R.drawable.circlered)
                    }else{
                        img_blink.setBackgroundResource(R.drawable.circlegreen)
                    }
                }
        }


        val mapFragment : SupportMapFragment = supportFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        btn_back_detailwisata.setOnClickListener {
            finish()
        }
        btn_maps.setOnClickListener {
            val strUri = "http://maps.google.com/maps?q=loc:$t_latwisata,$t_longwisata (" + intent.extras?.getString(
                "namawisata"
            ).toString() + ")"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(strUri))
            intent.setClassName(
                "com.google.android.apps.maps",
                "com.google.android.maps.MapsActivity"
            )
            startActivity(intent)
        }
        simpanwisata.setOnClickListener {
            simpanwisata()
        }
        btn_message.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("namawisata",tampungnama)
            val intent=Intent(this,Komentar::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }

    @SuppressLint("SetTextI18n")
    fun distance(_lat1: Double, _lon1: Double, _lat2: Double, _lon2: Double) {

        // The math module contains a function
        // named toRadians which converts from
        // degrees to radians.
        var lat1 = _lat1
        var lat2 = _lat2

        val dlat = Math.toRadians(lat2 - lat1)
        val dlon = Math.toRadians(_lon2 - _lon1)

        lat1 = Math.toRadians(lat1)
        lat2 = Math.toRadians(lat2)

        // Haversine formula
        // apply formulae
        // apply formulae
        val a = sin(dlat / 2).pow(2.0) +
                sin(dlon / 2).pow(2.0) *
                cos(lat1) *
                cos(lat2)
        val rad = 6371.0
        val c = 2 * asin(sqrt(a))
        val hasil = rad * c
        jarak.text = "±" + String.format("%.2f", hasil) + " KM"
    }

    override fun onMapReady(googleMap: GoogleMap) {

        //These coordinates represent the latitude and longitude of the Googleplex.
        val latitude = t_latwisata.toDouble()
        val longitude = t_longwisata.toDouble()
        val zoomLevel = 15f

        val homeLatLng = LatLng(latitude, longitude)
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(homeLatLng, zoomLevel))
        googleMap.addMarker(MarkerOptions().position(homeLatLng))
    }

    private fun View.blink(
        times: Int = Animation.INFINITE,
        duration: Long = 500L,
        offset: Long = 20L,
        minAlpha: Float = 0.0f,
        maxAlpha: Float = 1.0f,
        repeatMode: Int = Animation.REVERSE
    ) {
        startAnimation(AlphaAnimation(minAlpha, maxAlpha).also {
            it.duration = duration
            it.startOffset = offset
            it.repeatMode = repeatMode
            it.repeatCount = times
        })
    }

    private fun simpanwisata(){
        val namawisata: String = txt_namawisata_detailwisata.text.toString().trim()
        val latwisata: String = t_latwisata.trim()
        val lonwisata: String = t_longwisata.trim()

        val databaseHandler = Sqlite_DatabaseHandler(this)
        if(namawisata!="" && latwisata!="" && lonwisata!=""){
            val status = databaseHandler.addWisata(Sqlite_Model(namawisata, latwisata, lonwisata))
            if(status > -1){
                Toast.makeText(this, "Wisata tersimpan", Toast.LENGTH_LONG).show()
            }
        }else{
            Toast.makeText(this, "Wisata gagal tersimpan", Toast.LENGTH_LONG).show()
        }
    }

    fun imageViewToByte(image: ImageView): ByteArray? {
        val bitmap = (image.drawable as BitmapDrawable).bitmap
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        return stream.toByteArray()
    }

    private fun PassingData_WisataDetail(){
        val i = this.intent
        txt_namawisata_detailwisata.setText(i.extras?.getString("namawisata"))
        txt_deskripsidetailwisata.setText(i.extras?.getString("deskripsi").toString())
        txt_fasilitasdetailwisata.setText(i.extras?.getString("fasilitas").toString())
        Picasso.get().load(i.extras?.getString("gambar")).into(img_detailwisata)
        txt_ketinggian.setText((i.extras?.getString("ketinggian").toString()) + " mdpl")
        t_latwisata = i.extras?.getString("lat").toString()
        t_longwisata = i.extras?.getString("long").toString()
        twakturamai = i.extras?.getString("wakturamai")
        if(twakturamai=="-"||twakturamai==""){
            txt_kondisiwisata.text = "Biasanya tidak ramai"
            img_blink.setBackgroundResource(R.drawable.circlegreen)
        }else{
            val splitWaktu: Array<String> = twakturamai!!.split("-").toTypedArray()
            val split1 = Integer.parseInt(splitWaktu[0].trim())
            val split2 = Integer.parseInt(splitWaktu[1].trim())
            val firstHour: Calendar = Calendar.getInstance()
            val hour = firstHour.get(Calendar.HOUR_OF_DAY)
            if(hour in split1..split2){
                txt_kondisiwisata.text = "Biasanya ramai"
                img_blink.setBackgroundResource(R.drawable.circlered)
            }else{
                txt_kondisiwisata.text = "Biasanya tidak ramai"
                img_blink.setBackgroundResource(R.drawable.circlegreen)
            }
        }
    }


}